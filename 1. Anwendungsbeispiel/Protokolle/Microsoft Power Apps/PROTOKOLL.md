# Protokoll Power Apps

1. Einführung zu Mendix

    Power Apps hat eine sehr ausführliche Dokumentation, die auch in deutscher Sprache geschrieben ist ( https://docs.microsoft.com/de-de/powerapps/ ). Insgesamt gibt es drei verschiedene App Varianten.

    * Canvas-Apps
    * Modellbasierte Apps
    * Portale

    Die einzelnen App-Varianten wurden gut beschrieben, so dasss ich schnell erkannte, dass Canvas Apps meinen Anwendungsfall abdeckte.

    04.11.2021T13:18-17:20

    Zu Testzwecken habe ich eine SharePoint-Liste erstellt. Beim Verbindungsaufbau mit der SharePoint-Liste wurden automatisch drei verschiedene Screens erstellt. "BrowseScreen" zum Anzeigen, Suchen und Auswählen und Hinzufügen der Daten. "DetailScreen" für die Detailansicht eines Datensatzes. "EditScreen" zum Bearbeiten eines Datenelements.

2. Datenmodell umsetzen

    05.11.2021T13:38-17:40

    Tabellen können entweder mit Hilfe einer SharePoint-, Excel-Liste oder mit Dataverse erstellt werden. In meinem Fall habe ich Dataverse verwendet, um die Tabellen und Beziehungen untereinander zu erstellen. Das Erstellen von Tabellen war sehr einfach. Dataverse erstellt für jede Tabelle eine große Anzahl von Attributen, von denen ich nicht weiß, wo sie verwendet werden.
    Beim Erstellen der Attribute kann man ebenso entscheiden, ob sie erforderlich oder optional sind.
    Das Definieren von Beziehungen zwischen zwei Tabellen ist ebenfalls sehr einfach. Der klassische 1:1-Beziehungstyp wird nicht angezeigt (nur "Viele zu eins", "Eins zu viele", "m:n-Beziehung"). Zumal 1:1 ein Sonderfall von "viele zu eins" oder "einer zu vielen" ist, aber dennoch ist es nicht intuitiv. Die Primärschlüssel der Tabellen werden automatisch generiert.
    

3. GUI entwickeln
    
    Es gibt eine Sidebar, mit der es möglich ist, Bildschirme zu erstellen und Tabellen einzubinden. Viele Elemente der oberen Leiste stammen von den Office-Produkten (Excel, Word) von Microsoft. Insgesamt sind die Elemente sehr klar und strukturiert. Die verschiedenen UI-Elemente können in der Sidebar gefunden werden.
    Entscheidet man sich für ein UI-Element, so erscheint eine weitere Sidebar(rechts),um Kofigurationen vorzunehmen.
    Außerdem gibt es viele nützliche Vorlagen zum Erstellen von Bildschirmen.
    Die einzelnen UI-Elemente eines Bildschirms werden in einer detaillierten Baumstruktur abgebildet, was ziemlich gut ist.

    1. Bildschirme erstellen, Formulare mit Tabellen verknüpfen  

        10.11.2021T15:20-19:00

        Ich habe eine der Vorlagen für die Bildschirme verwendet.
        Die Formulare lassen sich über die rechts eingeblendete Sidebar konfigurieren. In Dropdown-Menü kann man die Tabelle einbinden. Man kann zwischen den verschiedenen Modi „Bearbeiten“, „Neu“ und „Ansicht“ auswählen. In meinem Fall war es notwendig, für den Antragsteller zwei Bildschirme zu erstellen, einen zum Bearbeiten einen anderen zum Anlegen eines neuen Antragstellers. Für die Fahrzeugdaten war lediglich eine Formularmaske zum Anlegen eines neuen Eintrags erforderlich.
        Die Bildschirme zum Erstellen von Antrags- und Fahrzeugdaten waren identisch aufgebaut. Power Apps bietet vorgefertigte Funktionen um die Daten aus dem Formular zu speichern.


    2. Bearbeitungsbildschirm des Antragstellers erstellen
    
        11.11.2021T14:40-16:10

        Ich habe die Notwendigkeit eines Bearbeitungsbildschirm bemerkt, als ich vom Fahrzeugdatenbildschirm zum Antragsdatenbildschirm zurückkehren wollte und keine Daten angezeigt wurden. Hier war es wichtig, die richtigen Daten anzuzeigen. Bei der Konfiguration des Formulars kann man als anzuzeigendes Element den letzten Submits des entsprechenden Formulars wählen.

    3. Manuelle Tabelleneinträge über Main Formular

        11.11.2021T16:20-18:00

        Es ist auch möglich, die Datentabellen manuell mit Daten zu füllen. An dieser Stelle bietet Power Apps ein Hauptformular für eine Tabelle im Hauptmenü an, welches standardmäßig angepasst werden muss. Zunächst wurden nur wenige Attribute der Tabelle angezeigt, die ich dann angepasst habe.

    4. Duplikate Datensätze vermeiden

        23.11.2021T13:30-16:35

        Beim Anlegen von Antragstellern ist mir aufgefallen, dass die gleichen Datensätze gespeichert wurden (Antragsteller mit gleichem Nachnamen und Vornamen). An dieser Stelle war es notwendig, vor dem Abschicken der Daten eine Validierungsfunktion zu schreiben, die einen Lookup in der Datenbank durchführt, um keine Duplikate zu erzeugen. Die Dokumentation zu den verschiedenen Funktionen war gut beschrieben und hat mir geholfen mein Problem zu lösen ( https://docs.microsoft.com/de-de/powerapps/maker/canvas-apps/functions/function-filter-lookup )

    5. Zusammenfassen der Daten

        24.11.2021T16:00-18:05

        Zur Zusammenfassung der  Antrags- und Fahrzeugdaten gibt es einen FormViewer, in dem man das entsprechende anzuzeigende Element konfigurieren kann. Da die verwendeten Formulare eindeutige Bezeichner hatten, konnten die zuletzt gespeicherten Daten einfach integriert werden.
        Ich verwende zwei FormViewer, einen zum Anzeigen der Antragsdaten und den anderen zum Anzeigen der Fahrzeugdaten.
    
    6. REST-Api für Wunschkennzeichen Prüfung einbinden 

        1. OpenAPI Spezifikation 2.0 erstellen

            02.12.2021T14:15-16:00

            Power Apps unterstützt nur OpenAPI Version 2.0 für benutzerdefinierte Konnector ( https://docs.microsoft.com/de-de/connectors/custom-connectors/define-blank ). Meine aktuelle Spezifikation ist Version 3.0. Daher musste ich in der REST-API-Anwendung mit Hilfe einer Dependency von Spring eine OpenAPI-Spezifikation mit Version 2.0 generieren.
        
        2. Benutzerdefinierten Konnector erstellen

            02.12.2021T14:35-15:45

            Insgesamt sind fünf Schritte notwendig, um einen benutzerdefinierten Konnector zu erstellen ( https://docs.microsoft.com/de-de/connectors/custom-connectors/define-openapi-definition#import-the-openapi-definition-for-power-automate-and-power-apps )

            * Allgemein: Hier werden allgemeine Information wie der Host und eine Beschreibung der REST-API angegeben

            * Sicherheit: Hier wird der Authentifizierungstyp ausgewählt.

            * Definition: Hier wird eine Aktion der REST-API mit den entsprechenden Parametern und die Antwort definiert.

            * Testen: Hier wird der Konnector erstellt und anschließend getestet

        3. Checkbox in Fahrzeugdaten-Bildschirm integrieren + API-Aufruf starten

            02.12.2021T14:49-16:37

            Die Checkbox wurde dem Fahrzeugdatenbildschirm hinzugefügt. Über das Register „Daten hinzufügen“ kann der Konnektor hinzugefügt werden. Den Konnektoraufruf habe ich in das Ereignis "onchange" für das Eingabefeld des Kennzeichens integriert. Hier habe ich eine Funktion geschrieben, die zuerst prüft, ob die Checkbox aktiv ist und dann den Aufruf an die API umsetzt. Der Benutzer wird dann benachrichtigt, wenn das Kennzeichen frei ist. Diese Benachrichtigung hängt vom Ergebnis der API ab.
        
        4. Weiter Button ausblenden, wenn Kennzeichen belegt ist

           13.12.2021T11:44-14:40 

           Mit dem Feld „DisplayMode“ kann der Modus zum Ein- und Ausblenden des Buttons eingestellt werden. Die Funktion, die für das Ereignis "onchange" aufgerufen wird, musste angepasst werden, sodass ich eine weitere Anweisung definieren musste, bei der ich den Wert der API-Antwort einer neuen Variablen zuweisen musste. Dies ist über die Funktion Set(Variable, Wert)“ möglich. Es ist also möglich, mehrere Anweisungen in einer IF-Klausel zu haben.

           Referenz: 
           
           https://docs.microsoft.com/de-de/powerapps/maker/canvas-apps/functions/function-set
           
           https://powerusers.microsoft.com/t5/Building-Power-Apps/Multiple-statements-in-IF-TRUE-branch/td-p/143615 

    7. Bug: Beim wiederholten Vorgang der KFZ Umschreibung werden keine Formulare und Daten angezeigt

        04.12.2021T15:10-17:15

        Wenn man den Prozess der KFZ Umschreibung einmal durchlaufen hat und ihn ein zweites Mal durchlaufen möchte, wurden keine Felder, geschweige denn Daten in den Formularen, angezeigt. Die Lösung bestand darin, das Ereignis "onvisible" mit dem Argument "NewForm (Form)" aufzurufen, das immer ein neues Formular erstellt, wenn dieser Bildschirm angezeigt wird.

     8. Abschlussseite

        14.12.2021T12:00-12:33   

        Ich habe einen neuen Bildschirm für die  Abschlussseite erstellt. Ein fertiges Checked Icon mit einer Beschreibung habe ich hinzugefügt. Außerdem habe ich einen Button eingefügt, der die Fahrzeugumschreibung beendet und auf die Startseite weiterleitet.

4. End-To-End Test entwickeln

    12.01.2022T Dauer 3h

    In der linken Seitenleiste findet man unter dem Menüpunkt Erweiterte Tools das Test Framework

    Voraussetzung: Die Applikation muss bereits veröffentlicht sein!

    Wenn man keine Lizenz für Produktion hat, kann man die 30 tätige Testversion nutzen.


    Da die Wunschkennzeichen Prüfung per Zufall erfolgt, wird in keinem Testfall ein Wunschkennzeichen angefragt.
    

    Interaktionen mit der Applikation können aufgezeichnet werden und daraus bildet sich dann ein Testfall, welcher dann manuell angepasst werden kann.
    Man kann Assertions einfügen. -> ähnlich zu Unit Testing


5. Debugging

    12.01.2022T

    Beim Entwickeln der Tests habe ich einige Anpassungen vorgenommen, wie beispielsweise die Auslagerung einiger Bedingungen auf globale Variablen. Diese Variablen wollte ich genauer untersuchen, weil oft Tests fehlgeschlagen sind.
    Unter den Erweiterten Tools gibt es ebenso, einen Monitor zum Überwachen der Applikation. Dieser fasst jede Interaktion auf und listet die Ergebnisse und viele weitere Parameter auf.
    Leider werden hier die Werte globaler Variablen nicht aufgezeichnet. Jedoch kann man die Werte der globalen Variablen über den Haupt Menüpunkt Ansicht-> Variablen der oberen Menüleiste sehen. Sobald man den Bildschirm wechselt, hat man keinen Zugriff mehr auf die Werte des vorherigen Bildschirms.



4. Sonstiges

    * Vorschau der App sehr schnell, insgesamt sehr flüssig
    * Nicht zu 100 Prozent erkennbar, in welchen Schritten eine App erstellt wird, nachdem man die grundlegenden Informationen wie den Namen angegeben hat.
    * Kein Default Screen möglich
    * UI Widgets sind frei bewegbar, keine Notwendigkeit von Containern



Nachtrag:

Problem 1: Dropdown Antragsteller im Dropdown setzen
Lösung 1: Default Property: Formular default und defaulSelectedItems werte setzen

Problem 2: Keine anzuzeigenden Daten bei Fahrzeugdaten -Formular bei erneutem KFZ Umschreibungsprozess
Lösung 2: NewForm(FormX) onVisible Property