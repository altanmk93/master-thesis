# Protokoll ServiceNow

1. Einführung zu ServiceNow

    12.11.2021T10:04-14:10

    ServiceNow App Engine Studio für Entwicklung
    UI Builder

    Dev Studio Seite für Entwicklung

    Die Entwicklung einer App ist in Schritten aufgeteilt

    * Data
    * Experience
    * Logic and automation
    * Security
    * Launch

2. Datenmodell umsetzen

    12.11.2021T14:40-18:00

    Beziehung zwischen Tabellen nicht intuitiv 
    Attribut muss als Typ Reference initialisiert werden
    Primärschlüssel ist sys_id (https://subscription.packtpub.com/book/networking-and-servers/9781782174219/1/ch01lvl1sec09/exploring-the-database)


3. Rolle anpassen

    12.11.2021T18:00-18:10

    Templates nicht editierbar
    Blank UI nicht möglich? -> Rolle ändern zu admin
    Deployment nur als admin möglich


3. Serverseitig Workspace einrichten

    17.11.2021T10:05-13:10

    am anfang komplett verwirrt, nicht verstanden wie und wo anzufangen
    erster Schritt UI entwickeln:

    1) Experience erstellen: hier beschrieben https://docs.servicenow.com/bundle/rome-application-development/page/administer/ui-builder/task/create-experience.html
    1.1) richtige App Shell für UI Builder auswählen: https://docs.servicenow.com/bundle/rome-application-development/page/administer/ui-builder/concept/app-shells-uibuilder.html / https://pishchulin.medium.com/now-experience-framework-app-shell-and-routing-e6cb6085d13
    2) document erstellen dann mit ui builder öffnen -> komplizierter weg
    3) Page erstellen: https://docs.servicenow.com/bundle/rome-application-development/page/administer/ui-builder/task/create-page.html


    css für styling möglich

    Problem: einige Event handler existieren auf meiner Seite nicht
    button redirect auf andere page nicht möglich
    lösung: Agent Workspace App Shell als UI Shell (statt Blank App Shell) bei der Now Experience


4. Client Skript entwickeln 

    18.11.2021T11:12-15:20
    problem: form fields zum bearbeiten von Feldern bei Antragsteller: this form has not been configured for workspace 
    um speichern eines records eigenes js script notwendig

    eigenes Formular zum Speichern von Records erstellt

    keine rückmeldung vom skript

    problem:input mit integriertem Datepicker nicht möglich (https://youtu.be/ak2cGXClaJ0?t=3280) laut ServiceNow Dev Program
    lösung: input format dd.mm.yyyy in yyyy-mm-dd mit js formatieren


5. Referenziertes Objekt übermitteln

    26.11.2021T10:45-14:55
    
    problem: um fahrzeug zu erstellen wird referenzierter antragsteller benötigt. sys_id von antragsteller muss ermittelt werden

    sys_id als URL Parameter senden-> Problem: sys_id kann nicht ermitteln werden

    1. Neuer Lösungsansatz: Business rules für CRUD (Empfehlung von Kollegen)

        27.11.2021T11:34-15:55
        Business Rule:
	    Festlegen wann eine Business Rule ausgeführt werden soll: after
	    Datenbank-Operation: Insert -> neuen Datensatz
	    Sys_id kann ermittelt werden
	    RedirectURL Methode, jedoch wurde kein Redirect auf dem Client ausgelöst

        keine Lösung!


7. Sonstiges

    * Probleme mit Deployment/Vorschauansicht: 
    https://community.servicenow.com/community?id=community_question&sys_id=d7f6081fdb1ba850190b1ea668961971&view_source=searchResult
    * Ui Komponenten Tree sehr übersichtlich aufgebaut
    * sehr ausführliche Dokumentation
    * deployment, review fehleranfällig/lange wartezeiten/ausfallfällig
    * datentabellen ansicht: studio dann die app auswählen
    * update sets für die versionierung


