# Protokoll Mendix

1. Einführung zu Mendix

    09.11.2021T09:45-14:10

    Mendix verwendet Mendix Studio, um Low-Code-Anwendungen zu entwickeln.
    Jede Änderung wird daraufhin geprüft, ob sie veröffentlicht werden kann. Es gibt eine Vorschauansicht, um Änderungen anzuzeigen, bevor sie bereitgestellt werden.

2. Datenmodell umsetzen

    09.11.2021T15:00-18:00

    Das Datenmodell ließ sich einfach per Drag & Drop einrichten. Der Aufbau von Beziehungen zwischen Entitäten kann auch mit einfachen Verbindungslinien realisiert werden.
    Die verschiedenen Arten von Beziehungen werden anschaulich mit Beispielsätzen erklärt.

    ![](images/beziehungstypen.PNG)

3. GUI entwickeln
    1. Wizard für die einzelnen Schritte

        19.11.2021T11:24-14:00

        Mendix bietet eine Toolbox mit verschiedenen UI Komponenten an. Es existiert auch ein fertiges Wizard, jedoch kann man nur wenig Anpassungen vornehmen. Wenn man einen weiteren Schritt im Wizard einbauen möchte, ist man gezwungen den Wizard selbständig neu aufzubauen. Man kann nur geringe Stylinganpassungen vornehmen.
        Klickt man auf eine UI Komponente, wird der entsprechende Tree angezeigt, das ist sehr praktisch, jedoch ist sie Ansicht nicht sehr detailliert.

    2. Antragsdaten- und Fahrzeugdatenformular

        Der Aufbau von dem Antrags- und Fahrzeugdatenformular wurde durch einen automatischen Prozess elegant gelöst, jedoch war es notwendig für meinen Fall die Aktionen der Speichern-Buttons mit Microflows zu lösen, weil ich mehr als nur das Speichern als Aktion für den Button benötigte.

        1. Custom Microflows für die Buttons definieren

            19.11.2021T14:20-16:40
            
            Insgesamt musste ich zwei Microflows definieren. Das erste für das Antragsdatenformular und das zweite für das Fahrzeugdatenformular.
            Beim ersten Microflow musste ich das Objekt für den Antragsteller in der Datenbank speichern, ein neues Fahrzeug Objekt erstellen und es auf die nächste Seite (Fahrzeugdatenformular) weiterleiten. Die Toolbox verwendet an dieser Stelle sehr verständliche graphische Objekte, so dass ich das Problem schnell lösen konnte. 

        2. Assoziation selbständig in Custom-Microflows setzen

            20.11.2021T14:00-18:00

            Ich hatte das Problem, dass die mit dem Antragsteller verknüpften Attribute des Fahrzeugs nicht angezeigt wurden. Es reichte nicht aus, das Fahrzeug Objekt zu speichern, weil es keine Referenz zum Antragsteller hatte.
            Nach langer Recherche bin ich auf einen Forenbeitrag gestoßen, in dem beschrieben wurde, dass man auch das verknüpfte Attribut des Fahrzeugs setzen muss.
        
            https://forum.mendix.com/link/questions/85692
        
    3. Zusammenfassen der Daten

        25.11.2021T12:00-15:20

        Der Microflow, der nach der Bearbeitung der Fahrzeugdaten aufgerufen wird, leitet das erstellte Fahrzeugobjekt an die Übersichtsseite weiter. Über das Datenansichts-Widget kann das Fahrzeugobjekt als Datenquelle eingebunden und dessen Attribute in der Ansicht angezeigt werden.

    4. Abschlussseite

       26.11.2021T15:25-17:30

       Die Abschlussseite ist ähnlich wie die anderen Seiten gestaltet. Der Wizard steht ganz oben und sein letzter Eintrag ist farblich hervorgehoben. Es folgen drei weitere Elemente, der Titel, ein Kurztext und ein Button, der die Umschreibung des Fahrzeugs beendet. Leider konnte ich kein vorgefertigtes Checked Icon finden. In einigen Foreneinträgen wird erklärt, wie man Icons importieren kann.( https://www.mendix.com/blog/import-custom-icons-mendix/ ) 
       
3. REST-Api für Wunschkennzeichen Prüfung einbinden

    Die Anwendung wurde bis vor der Integration der API mit dem webbasierten Mendix Studio entwickelt. Für die Integration einer API in die Applikation ist jedoch die Desktoplösung Mendix Studio Pro notwendig.

    Mendix bietet auf der offiziellen Dokumentationsseite eine Anleitung, wie eine REST API konsumiert werden kann: https://docs.mendix.com/howto/integration/consume-a-rest-service

    Es existieren ebenso gute Video Tutorials: https://www.youtube.com/watch?v=KCyb1lrZ8NE , https://www.youtube.com/watch?v=NJD4DS0Rv3o 
    

    1. Datenmodell anpassen

        05.12.2021T11:20-12:21

        Der Nutzer soll mit einer Checkbox entscheiden, ob er ein neues Kennzeichen beantragen möchte. Diese entsprechende Variable muss als Attribut in der Entität gesetzt werden. Es ist nicht möglich, den Status der Checkbox innerhalb eines Formulars einer Variablen statt dem Attribut der entsprechenden Entität zuzuweisen.

    2. JSON-Struktur erstellen

        05.12.2021T12:21-13:25

        Die JSON-Struktur der Response der REST API muss definiert werden. Hierfür kann man ein Beispiel JSON-Objekt in das Eingabefeld einfügen, das danach die Struktur definiert.

    3. Microflow erstellen + API Call konfigurieren 

        05.12.2021T14:00-17:05

        Der Microflow sollte immer aufgerufen werden, wenn sich der Wert des Eingabefeldes für das Kennzeichen ändert. In den Microflow kann ein REST-API-Aufruf integriert werden. Dieser muss dann konfiguriert werden. Mit einem Doppelklick erscheint ein neues Fenster, in dem man die URL der API einbinden kann. Außerdem muss das KFZ-Kennzeichen als Eingabeparameter der URL eingestellt werden.

    4. Import Mapping erstellen

        05.12.2021T17:25-18:00

        Das Import-Mapping für die Antwort muss in einem anderen Reiter der Konfiguration erstellt werden.
        (Import-Mapping: https://docs.mendix.com/refguide/import-mappings). Wenn das Imprt-Mapping erstellt wird, kann hier die JSON-Antwortstruktur eingestellt werden. Das Import-Mapping ist hier notwendig, um die JSON-Antwort der API einer Entität zuzuordnen. Mendix erstellt an dieser Stelle eine nicht-persistente Entität.

    5. Weiter-Button ausblenden, wenn das Kennzeichen nicht verfügbar ist
        
        09.12.2021T12:45-15:45

        Die API-Antwort konnte zwar in einer Variablen gespeichert werden, war aber nur im Rahmen des entsprechenden Microflows sichtbar.
        Der Weiter-Button kann nach einer Bedingung ausgeblendet werden. Das sichtbare Objekt in dieser Umgebung war das Fahrzeugobjekt. Da globale Variablen in Mendix nicht existieren und Daten prinzipiell nur in Entitäten gespeichert werden, war es notwendig ein neues Attribut für das Fahrzeug zu definieren( https://stackoverflow.com/questions/55794490/how-to-declare-a-global-variable-in-mendix ). Der Wert des Attributs konnte im Microflow der API gesetzt werden. Dazu gibt es die Objektaktivität "change Object", mit der man das Fahrzeugobjekt entsprechend der Antwort der API-Antwort anpassen konnte. Insgesamt scheint diese Lösung nicht der eleganteste Weg zu sein. 

        09.12.2021T15:45-16:15 Merge-Konflikt lösen

        Ich habe bei der Entwicklung dieser Lösung den Fehler gemacht, dass ich nicht mit dem aktuellsten Stand lokal gearbeitet hatte. Für Testzwecke habe in der Cloud-basierten Umgebung Mendix Studio einen neuen Stand entwickelt. Nachdem ich meine Lösung implementiert hatte, musste ich zunächst den Merge-Konflikt lösen. Mendix Studio Pro hat den Konflikt schnell erkannt, da es mit Mendix Studio synchronisiert wird.
        Mendix Studio Pro zeigt genau, welche Änderungen im Konflikt aufgetreten sind. Der Konflikt konnte somit leicht gelöst werden.

        Referenz: https://docs.mendix.com/refguide/new-merge-algorithm

4. Weiterentwicklung
    
    1. Validierung Eingabefelder 

       15.12.2021T15:05-17:00

       https://docs.mendix.com/howto/data-models/setting-up-data-validation

       Für die Plz wurde ein regulärer Ausdruck definiert. Alle anderen Felder wurde eine Validierungsregel eingeführt, dasss sie unausgefüllt sein dürfen.
    
    2. Geschlecht als Entität darstellen

        16.12.2021T15:25-17:45
    
        Anpassungen am Datenmodell müssen mit Bedacht gemacht werden. Ich habe eine Validierungregel eingefügt, die nur einzigartige Namen des Antragstellers erlaubt. Dadurch ist das Deployment fehlgeschlagen.
        Da Deployments in der Regel hier nicht sehr zuverlässig verlaufen, wusste ich nicht, ob ein echtes Problem vorliegt, bis ich dann eine Möglichkeit gefunden hatte, in die Logs zu schauen. Somit konnte ich das Problem lösen.

        ![](images/deployment-error.PNG)

        Es gibt keine schnelle Lösung für das Einfügen von initialen Daten in die Geschlechter-Tabelle. Mendix empfiehlt, dass man ein Microsoft Excel Spreadsheet erstellt und das dann importiert.
        
        Referenz:
        
        https://stackoverflow.com/questions/62677317/
        
        https://docs.mendix.com/studio/start-with-data 

        
    6. Sonstiges

        Das Deployment dauert in der Regel sehr lange und scheitert in einigen Fällen. Im Fehlerfall weist Mendix auf die entsprechende Stelle hin. Ich habe keine Möglichkeit gefunden, die Einträge in den Datentabellen anzuzeigen.

        Mendix zeigt an, wenn ein UI-Element nur innerhalb eines Widgets funktioniert. Checkboxen funktionieren nur in der Datenansicht oder Listenansicht

        Mendix Studio Pro:
        - Debugging: Setzen eines Breakpoints ist möglich
        - Versionskontrolle integriert
        - Versionierung: https://cloud.home.mendix.com/index.html?0001