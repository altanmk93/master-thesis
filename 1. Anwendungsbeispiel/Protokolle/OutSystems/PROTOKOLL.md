# Protokoll Outsystems

Für die Umsetzung des ersten Anwendungsbeispiels wurden die Ziele und Teilprobleme in den folgenden Schritten aufgelistet.

1. Einführung zu Outsystems
   
    02.11.2021T11:45-14:00

    Ich habe den Fehler gemacht, ohne Vorkenntnisse selbstständig mit der Plattform zu arbeiten. Es war sehr frustrierend und zeitaufwändig, die Plattform zu verstehen.

    02.11.2021T14:15-16:00

    Da ich noch keine Erfahrung mit Low-Code-Plattformen habe, bin ich auf der Outsystems Dokumentationsseite auf einen hilfreichen Artikel gestoßen, der knapp in wenigen Schritten beschreibt, wie man seine erste reaktive Webapplikation entwickelt. (https://success.outsystems.com/Documentation/11/Getting_started/Create_Your_First_Reactive_Web_App). Es wurden viele Screenshots eingebunden, die für mich sehr hilfreich waren. Ich habe die erwähnten Schritte fast 1:1 nachgemacht.


2. Datenmodell umsetzen
    1. Entitäten erstellen und Beziehungen zwischen Entitäten aufbauen

        03.11.2021T11:20-14:35

        Der Artikel zur Erstellung einer Web-App war zwar hilfreich, jedoch wird hier nicht erklärt, wie man Beziehungen zwischen Entitäten erstellt. Für die Umsetzung des Datenmodells habe ich in der Dokumentationsseite von Outsystems einen anderen Artikel gefunden, der beschreibt, wie man Entitäten erstellt und Beziehungen zwischen erstellt (https://success.outsystems.com/Documentation/11/Developing_an_Application/Use_Data/Data_Modeling/Entity_Relationships).

        An sich ist die Umsetzung der Beziehung sehr simpel und verständlich aufgebaut. Jedoch gibt es noch einen weiteren Weg, der hier aber nicht beschrieben wurde.
        Für die Umsetzung des Datenmodells habe ich die im Artikel beschriebenen Werkzeuge verwendet. Die Entitäten und die zugehörigen Beziehungen ließen sich problemlos umsetzen.

3. Kurzen Ausschnitt von Berliner Staßen aus einer Excel Datei importieren
    1. Fehler in der Datenbank identifizieren und beheben
        
        03.11.2021T17:30-18:05

        In der ersten Version meines Datenmodells habe ich eine Entität für Adressen erstellt, welche ich dann im Nachhinein angepasst hatte. In der neuen Version habe ich einige Attribute zusammengefasst. Beim Anpassen der Datenmodells ist mir ein Fehler passiert. Da bereits Daten in der Datenbank persistiert wurden, ist das Deployment fehlgeschlagen. Die entsprechende Fehlermeldung war nicht sehr genau beschrieben, so dass ich das Problem mit eigener Idee gelöst hatte. 

        ![](images/database-update-fehler.PNG)         

    2.  Import Berliner Straßen
        
        03.11.2021T18:10-18:20
        
        Der Import der Daten aus der Excel Datei erfolgte problemlos. Outsystems hat die Entität nach dem Namen des Reiters benannt. Die Daten wurden ebenso mit den richtigen Datentypen importiert.

4. Antrags-und Fahrzeugdatenformular umsetzen

   10.11.2021T10:30-14:50 

   Versuch selbsständig UIs umzusetzen
   
   11.11.2021T10:25-14:30

   Outsysystems hat eine sehr hilfreiche Funktion, indem eine Entität per Drag & Drop in das Bildschirm gezogen wird. Anschließend wird ein Formular generiert, die alle Attribute der Entität enthält. Somit war das Antragsformular sehr einfach zu erstellen. Außerdem wurde auf einem separaten Bildschirm automatisch ein Formular zum Bearbeiten und eine Tabelle mit allen Einträgen generiert.

   1. Verknüpfung zwischen Antragsteller und Fahrzeug setzen 

        16.11.2021T10:05-15:15 

        Es bestand noch das Problem, dass man den Antragsteller im zweiten Schritt beim Ausfüllen der Fahrzeugdaten manuell im Dropdown Menu setzen musste. 
        Lösung: Mit Outsystems ist es möglich, sogenannte Eingabeparameter zu setzen. Dies sind Pfadvariablen einer URL. Es war notwendig, diese Variable nach dem Anlegen eines Antragstellers im UI-Flow des Antragstellers zu setzen und dann an den nächsten Bildschirm zu senden.
        Im zweiten Schritt war dann die ID des Antragstellers bekannt. Mit Hilfe einer Aggregatfunktion konnte der relevante Antragsteller aus der Datenbank ermittelt und die Verknüpfung gesetzt werden.

        Hinweis: Das Konzept mit den Eingabeparametern war mir bisher nicht bekannt. Erst nachdem ich die Dokumentation gelesen und mich durch die Anwendung geklickt habe, habe ich es verstanden.
        Ich hatte noch das Problem, dass die ID des Antragstellers und des Fahrzeugs anfangs immer Null waren und somit keine Datenbankeinträge angezeigt wurden. Nach langer Recherche habe ich herausgefunden, dass die ID nach Ausführen einer Serveraktion (Create oder Update) der entsprechenden Entität ermittelt werden kann.


        Bug: Verknüpfung richtig setzen

        17.11.2021T14:04-16:30 

        Ich ging davon aus, dass die Verknüpfung richtig eingestellt ist. Beim Nachschlagen in der Fahrzeugtabelle habe ich jedoch keinen Eintrag für den verlinkten Antragsteller gefunden. Das Problem war, dass ich in den Properties des Dropdown-Menüs die falsche Variable eingestellt habe. Die notwendige Variable war die Variable, die den Fremdschlüssel repräsentiert (GetFahrzeugById.List.Current.Fahrzeug.AntragstellerId)
        An dieser Stelle nutzte ich das Debugging von Outsystems, um zu prüfen, ob die Variable richtig gesetzt wurde.

5. Zusammenfassen der Daten

    24.11.2021T12:20-15:55

    Um die Daten zusammenzufassen, war ein neuer Bildschirm erforderlich. Zum Anzeigen der Daten wurde als UI-Element eine Liste für den Antragsteller und das Fahrzeug verwendet.
    Auch hier war es notwendig, die IDs der beiden Entitäten zu kennen. Daher wurden sie im zweiten Schritt als Eingabeparameter übergeben.
    Mit den Ids musste ich vier Aggregatfunktionen schreiben, um den Antragsteller selbst, die Fahrzeugdaten, die Adresse und das Geschlecht des Antragstellers zu ermitteln.

6. REST-Api für Wunschkennzeichen Prüfung einbinden

    Es gibt zwei Möglichkeiten, eine REST-APi zu konsumieren.
    Die erste Möglichkeit besteht darin, eine einzelne Rest-Api-Methode zu integrieren, die zweite Möglichkeit besteht darin, mehrere Methoden einer REST-API zu verwenden. Hier muss jedoch die API-Spezifikation als json-Datei hochgeladen werden.

    1. API-Spezifikation implementieren

        30.11.2021T10:05-11:25

        Auch wenn ich nur eine einzige Methode in meinem Wunschkennzeichenprüfungsservice habe, habe ich mich entschieden, eine API-Spezifikation zu implementieren, die ich dann hochgeladen habe.

    2. Response Struktur definieren

        30.11.2021T11:25-12:35
        
        Mir war nicht klar, wie man eine Struktur erstellt. Lange probiert, dann eine API mit entsprechender Beispieleingabeparameter genutzt und Struktur 1:1 nachgebildet.
        Im nächsten Schritt war es notwendig, eigenständig eine Response zu definieren, da die API nur Daten liefert, wenn sie mit einem Eingabeparameter aufgerufen wird. Vorher war in dieser Umgebung die Struktur der Ausgabe nicht bekannt. Ich habe die Struktur mit entsprechenden Attributen 1:1 nach der API-Spezifikation definiert.

    3. Ergebnis in Outputparameter speichern

        30.11.2021T13:35-15:50

        Das Ergebnis der Anfrage muss in einer Ausgabevariable gespeichert werden. Ich habe dies in einem Forenbeitrag gelesen (https://www.outsystems.com/forums/discussion/40054/how-to-get-response-code-of-a-rest-api-post-method/). Als Datentyp habe ich die im vorherigen Schritt definierte Struktur zugewiesen. So konnte ich den API-Aufruf per Drag & Drop in einen UI-Flow integrieren.

    4. Checkbox im Fahrzeug-Bildschirm integrieren

        01.12.2021T16:00-16:30

        Ich habe auf dem Bildschirm der Fahrzeugerstellung eine Checkbox integriert, die gecheckt werden soll, wenn der Nutzer sich ein neues Kennzeichen wünscht. Der Wert der Checkbox muss in einer lokalen Variablen gespeichert werden.

    5. UI Flow für API-Aufruf umsetzen

        01.12.2021T16:30-18:15

        Es ist notwendig einen UI Flow umzusetzen, welcher immer dann aufgerufen wird, wenn der Nutzer das Eingabefeld verlässt. Hier wird wenn die Checkbox gesetzt ist, der API-Aufruf mit gewünschtem Kennzeichen realisiert und anschließend wird der Benutzer benachrichtigt, ob das Kennzeichen frei ist. 

    6. Weiter Button ausblenden, wenn Kennzeichen belegt ist

       08.12.2021T13:05-15:25 

       Um den Button bei einem gewünschten Kennzeichen zu deaktivieren, bis die API-Antwort anzeigt, dass das Kennzeichen vorhanden ist, war es notwendig, diesen Status in einer Variablen zu speichern. Dieser Zustand wurde im Flow zugewiesen. In den Properties des Buttons habe ich einen Ausdruck gesetzt, der den Button im Fall eines Wunschkennzeichens deaktiviert, wenn das Kennzeichen nicht frei ist.
        
 7. Abschlussseite

    14.12.2021T14:05-15:37

    Für die Abschlussseite habe ich einen neuen Bildschirm im MainFlow erstellt. Ein Doppelklick auf den Bildschirm führt direkt zu diesem Bildschirm. Dort habe ich ein von Outsystems zur Verfügung gestellstes Check Icon mit einem Text darunter eingefügt. Außerdem habe ich einen Button zum Beenden der Fahrzeugumschreibung hinzugefügt, welches zum Homescreen weiterleitet. Letzteres wurde mit einem Flow ausgelöst. Die UI-Elemente wurden zentriert angeordnet. Anfangs fiel es mir schwer, die Elemente in dieser Form zu platzieren, bis mir das Widget "center content" aufgefallen ist, mit dem ich es letzendlich lösen konnte.
        
8. Sonstiges

    Das Deployment läuft sehr schnell und ohne Probleme.
