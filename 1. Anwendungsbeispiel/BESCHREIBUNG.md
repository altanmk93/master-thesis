# Fahrzeug Umschreibung

* Version 1: Antragsprozess

    * Der Bürger gibt seine persönlichen Daten zur Identifikation in einer Antragsformular ein und lädt gegebenenfalls die erforderlichen Dokumente (Ausweisdokument) hoch
    * Die Plattform validiert die Eingabefelder (z.B. PLZ)
    * Der Bürger erhält einen Überblick über seine Daten, damit er die Möglichkeit hat, Korrekturen vorzunehmen
    * Er bestätigt die Richtigkeit der Daten und wird auf eine Abschlussseite weitergeleitet wo er den Antrag schließen kann

* Version 2: Wunschkennzeichenprüfung über einen Service

    * Die Plattform bietet auch die Möglichkeit, ein Wunschkennzeichen als neues KFZ-Kennzeichen zu verwenden
    * Der Bürger entscheidet, ob er sein altes KFZ-Kennzeichen behalten oder ein neues beantragen möchte
    * Entscheidet sich der Bürger für ein neues Wunschkennzeichen, kann er sein Wunschkennzeichen eingeben und es wird geprüft, ob es noch verfügbar ist (an dieser Stelle wird eine API verwendet)
    * Entscheidet er sich dafür, dass er sein altes Kennzeichen behalten möchte, so gibt er es in einem Eingabefeld ein