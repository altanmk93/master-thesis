1. Import Bewerber, Stellen aus Excel

    1. Datenmodell umsetzen
        11.02.2022
        Date Datentyp existiert nicht -> nur Date and time möglich

        Bei Many to Many Relationen wird zusätzliche Tabelle automatisch erstellt. 

    2. Excel Importer Modul für Import verwenden
    
    11.02.2022
    Mendix bietet per Default keine Möglichkeit zum Upload. Empfohlen wird das Modul "Excel Importer" aus dem Marketplace. Mendix ist selbst der Entwickler dieses Moduls.
    Nach Installation enthält das Modul 377 Errros! 
    -> Mx Model reflection Module muss zusätzlich installiert werden
    -> Empty caption error bei verschiedenen Sprache
    -> default Sprache muss auf Englisch, US gesetzt werden
    Referenz: https://forum.mendix.com/link/questions/2052

2. Listen und Formulare erstellen

    11.02.2022
    Für Listen Data Grid als Widget nutzen 
    Für New/Edit Page Template Page mit Form Horizontal nutzen

3. Sicherheitsrichtlinien für Datenmodell anpassen

    11.02.2022
    Nachdem ich die Umgebung auf Produktion umgestellt hatte, hatte ich das Problem dass das Leserecht der Attribute von den Entitäten für den Nutzer User verweigert wurden. Die Lese- und Schreibrechte musste ich manuell für den Nutzer "User" freigeben. 
    Unter access rules beim anklicken auf enität unter domain model

4. Zuordnung Bewerbung -> Stelle

    11.02.2022

    Verschiedene Lösungen versucht: Neue Seite mit Data view und reference select
    -> nicht funktioniert

    Lösung war: bootstrap multi select in form einbinden

    Problem: New und delete button in listen ausgeblendet
    Lösung: access rules für "User" anpassen, erlauben zum erstellen und löschen von objekten

5. Listenansicht Bewerber und Stelle

    12.02.2022
    Das Problem ist, dass im Domain Model keine erkennbare dritte Tabelle durch die N:M Relation zwischen den Tabellen Bewerber und Stelle erzeugt wurde, so dass man bei list views oder data view diese Tabelle als Datenquelle nutzen kann.

    List View bei Many-To-Many Assoziation nicht existent
    Lösungshilfen hier beschrieben:
    https://forum.mendix.com/link/questions/96622 
    In den Listen eine neue Spalte hinzufügen und referenziertes Attribut zuordnen.

6. Bewerbungsstatus und Angebot Formular erstellen

    21.02.2022

    Da Mendix bei N:M Beziehungen keine physische Tabelle für Bewerbungen erstellt, muss man die Beziehungen direkt über die Entitäten Bewerber und Stelle aufbauen.

    Beim erstellen eines neuen Branches wird die lokale Datenbank nicht synchronisiert!
    -> nicht gut!

    Der Branch wird aus einer Revision vom Team Server (Stand vom Server)erstellt, daher wird das Datenmodell und die Datenbank aus dem Server sychronisiert
    -> branch aus lokalen Stand erstellen nicht möglich

7. Java Action für Berechnung von Dashboardzahlen definieren

    18.03.2022

    Mendix Anwendungen werden mit Java entwickelt.
    Nachdem Erstellen einer Java Action kann man explizit definieren welche Parameter und ob einen Rückgabewert die Methode haben soll.
    Als Parameter können auch die über Mendix Studio Pro erstellten Entitäten übergeben werden.
    Das gesamte Projekt lässt sich in Eclipse importieren, somit hat man Zugriff an den gesamten Code.
    Nach Code Besichtigung wurde kein erkennbares Framework wie Spring etc. festgestellt.
    Die Java Actions liegen unter dem Ordner "moduleName.actions". Dort gibt es eine (override) Methode die die Logik der zu implementierenden Java Action darstellt.
    -> Java Action wurde umgesetzt und können über einen Microflow aufgerufen werden

    Anpassungen wie Parameteranpassungen in Mendix Studio Pro können über Run deploy for eclipse aktualisiert werden. Das Projekt in eclipse muss anschließend aktualisiert werden.

    1. Dashboardzahlen anzeigen

        18.03.2022

         Zahlen können nur mit einer Datenansicht(data view)-Widget angezeigt werden. Dazu wurde eine nicht persistente Entität (Bewerberquoten) mit den entsprechenden Attributen angelegt. Die Java-Aktion berechnet die Quoten aus und übergibt das entsprechende Bewerberquoten-Objekt als Rückgabewert. Dieses Objekt wird dann im Dashboard angezeigt.

Sonstiges:
* CI/CD:
https://www.mendix.com/evaluation-guide/app-lifecycle/cicd/ 
* Für jedes Modul muss man die Sichtbarkeit der Seiten für eine Benutzer einrichten
* Keine erkennbare Struktur der UI-Elemente
* Insgesamt weist eine Many-To-Many einige Schwierigkeiten für eine Darstellung und Zuweisung der Daten auf
* Microflow kann mit Custom Code (Java) erweitert werden anderen