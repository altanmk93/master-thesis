0. Datenmodell modellieren
    1. Excel-Tabellen mit Beispieldaten erstellen

        Namen hier generiert: https://www.behindthename.com/ 

    2. Datenmodell in Dataverse umsetzen
            Bei 1:1 Beziehungen muss man eine neue Spalte mit dem Datentyp Suche(Lookup) erstellen. Eine direkte 1:1 Beziehung existiert nicht

            Dataverse Tabellen haben eine primäre Namensspalte die in Formularen etc angezeigt wird.

1. Model-Driven App erstellen

    06.02.2022T

    Zunächst wurde eine Lösung in Power Apps erstellt. In einer Lösung können verschiedene Komponenten wie eine modellgesteuerte oder eine Canvas App, Datentabellen etc. erstellt/hinzugefügt werden.

2. Import Bewerberliste aus Excel in Bewerber Dataverse Tabelle

    06.02.2022T

    - keine Möglichkeit der Automatischen Generierung von Tabellen durch Import von Excel Datei

    Problem beim Import:
    Warning: The following source file columns have not been mapped to Bewerber fields: Vorname, Geburtsdatum, Email, Position, Abteilung, Vorgesetzter

    -> keine genaue Info
    -> Import ist nicht fehlerfrei (benutzerdefinierte Spalten können nicht zugeordnet werden)
    -> importierten Datensätze manuell bearbeiten -> nicht gut!
    scheint generelles problem zu sein:
    https://powerusers.microsoft.com/t5/Building-Power-Apps/CRM-fields-not-showing-up-when-mapping/td-p/425342
    https://powerusers.microsoft.com/t5/Microsoft-Dataverse/Custom-columns-not-appearing-import-mapping/m-p/752260#M8336 

    -> Lösung: Power Apps bietet eine alternative Möglichkeit eine Excel Datei herunterzuladen, die die entsprechende Dataverse-Tabelle darstellt. Dieser Tabelle wird dann mit der Umgebung verbunden (Login notwendig). Anschließend kann man diese Tabelle mit Daten befüllen und anschließend veröffentlich. Über diesen Weg können Daten importiert werden.

3. Listenansichten in modellgesteurter App

    19.02.2022

    Es ist notwendigt, das Datenmodell korrekt zu modellieren und umzusetzen. Dataverse bietet viele Funktionalitäten out-of-the-box. Die Ansicht und Formulare der Tabellen können modelliert werden. Die Tabellen können dann in der modellgesteuerten App über den App Designer verwendet werden. Über die Site Map kann man die Tabellen hinzufügen.
    Dort kann man auch die Formulare einbinden

    Ich hatte folgendes Problem:
    https://powerusers.microsoft.com/t5/Building-Power-Apps/My-table-doesn-t-reference-a-form-or-view/td-p/947513
    
    Hat sich aber durch die Tipps der Forenbeitrge lösen lassen.
    Man muss Formulare explizit einbinden. Durch die Validierungsfunktion des App Designers lassen sich potentielle Fehler schneller auffinden

4. Formulare erstellen 

    23.02.2022

    Bei N:N Beziehungen kann man die referenzierten Daten über den Reiter "Related" ansehen, wenn man vorher auf einen Datensatz klickt. Eine Anpassungen hier im Formular ist nicht notwendig

    Bei 1:N Beziehungen muss man im Hauptformular einen Unterraster hinzufügen, der den verknüpften Datensatz anzeigt. Mit diesem Unterraster ist es möglich, mehrere Datensatze zu erstellen.

    Dataverse verwendet verschiedene Ansichten/Formulare. Beispielsweise gibt es für Assoziationen eine extra Ansicht.

4. Dashboard mit Statisken

    19.02.2022

    In der App werden keine Diagramme zu den Listen angezeigt
        ->Diagramme müssen über Dataverse erstellt werden.

    Beim Erstellen von Diagrammen lassen sich keine Filter anwenden.

    In einem Kreisdiagramm sollen die Anzahl der Bewerber nach Positionsbezeichnung angezeigt werden.
        -> nicht möglich, da Diagramme die Ansichten von Dataverse für die Erstellung des Diagramms nutzen. Da keine N:N Abbildungen in den Ansichten dargestellt werden, ist diese Art von Diagramm nicht möglich.
        -> Diagrammerstellung über Dataverse nicht besonders flexibel-> besser geeignet wäre PowerBI

5. Custom Code integrieren

    19.03.2022

    Bei modellgetriebenen Anwendungen kann ClientSeitiges Skripting mit JavaScript als Script-Webressource verwendet werden.
    Referenz: https://docs.microsoft.com/de-de/powerapps/developer/model-driven-apps/supported-customizations 

    https://docs.microsoft.com/de-de/powerapps/developer/model-driven-apps/client-scripting 
    JavaScript Code kann zu einem existieren Formularskriptereignishandler(OnSave, OnChange etc.) augerufen werden.
    Beispiel eines JavaScript ClientSkriptes:
    https://docs.microsoft.com/de-de/powerapps/developer/model-driven-apps/clientapi/walkthrough-write-your-first-client-script?tabs=unified-interface%2Cunified-add-web-resource

    Debuggen des ClientSkripts
    https://docs.microsoft.com/de-de/powerapps/developer/model-driven-apps/clientapi/debug-javascript-code

    weitere Möglichkeit Js Code einzubinden:
    Webressourcen(z.B statische HTML Seite, XML etc.) in Formularen integrieren


5. Sonstiges:

    * Datenvisualisierung mit Power BI-Dienst
    * Power Apps hat eine relativ gute Community