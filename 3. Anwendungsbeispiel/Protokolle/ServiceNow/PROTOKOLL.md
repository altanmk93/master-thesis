1. App erstellen
    
    12.02.2022
    
    Application Guided Creator genutzt
        -> hier gibt es Möglichkeit Daten zu importieren
        -> Datentabellen automatisch nach Import erstellt, manuelle Anpassungen auch möglich 

2. Datenmodell umsetzen
    
    12.02.2022

    Studio IDE verwendet: hier werden alle Tabellen der Anwendung aufgelistet 
        -> mehr Kontrolle über die Anwendungskomponenten
        -> Beziehungenspalten müssen manuell in Tabellen erstellt werden
        -> 1:N Beziehung: Tabelle um Referenzattribut erstellen
        -> N:M Beziehung: hierfür gibt es in IDE eine eigene Lösung: Angabe beider Tabellen genügt

3. UI umsetzen
    
    15.02.2022
    
    Experience Vorlage Workspace verwendet
        Tabellen auswählen -> erstellt automatisch Listenansicht, Formulare etc.

    1. Dashboard erstellen

        15.02.2022 

        Im UI Builder gibt es eine Komponente 'Data visualization' mit der Diagramme/Kennzahlen konfiguriert werden können
            -> sehr einfache Umsetzung von Diagrammen
            -> Tabelle auswählen, Diagrammtyp auswählen, Aggregatfunktionen anwenden, Daten können gefiltert werden, Diagramm wird automatisch generiert
                -> allerdings kann nicht jede Art von Diagramm für eine Tabelle erstellt werden!

4. Benutzerdefinierter Code

    16.02.2022
    
    JavaScript Code kann als in Geschäftslogik oder Client-Skript eingesetzt werden
    -> Kein Anwendungsfall gefunden