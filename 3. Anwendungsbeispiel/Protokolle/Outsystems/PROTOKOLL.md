1. Reaktive Webapplikation erstellen

    09.02.2022T

2. Import des Excel-Datensatzes

    09.02.2022T

    Der Import hat ohne Fehler funktioniert. Es ist möglich, mehrere Entitäten aus einer einzigen Excel-Datei zu generieren. Es ist wichtig, dass man die Tabellenblatt in Excel so benennt, wie die Tabelle in der Anwendung benannt werden soll.

3. Beziehungen zwischen den Datentabellen aufbauen

    09.02.2022T

    One-One-Relationship
    https://www.youtube.com/watch?v=_U_W5W26JTU

    Datentyp der Entitä als ID der referenzierten Entität setzen und Delete Rule als Delete setzen

4. Formulare zum Erstellen/Bearbeiten von Bewerbern/Stellen etc. erstellen

    09.02.2022T
    Drag & Drop Prinzip erstellt alle Felder zum Bearbeiten und Erstellen einer Entität, sowie die Listen zum Anzeigen der Tabelleneinträge. Die Listen enthalten ein Suchfeld, sowie eine Schaltfläche zum Öffnen der Formular zum Hinzufügen eines neuen Eintrags. 

5. Formular Bewerbungsstatus, Bewerbung, Angebot

    19.02.2022T

    Durch Anpassung des Datenmodell inkonsistente Daten in Tabelle Bewerbung erreicht
    -> Datentabellen duplizieren, betroffenen Enitäten löschen und Datentabellen neu erstellen

        
    Problem bei Bewerbungstatusliste Ansicht Bewerbung wird nicht angezeigt:

        -> Lösung Join von Bewerbung Tabelle

6. Dashboard mit Diagrammen erstellen

    19.02.2022

    Outsystems besitzt Vorlagen für Dashboards, von der ich Gebrauch gemacht habe. Diese müssen aber angepasst werden. Für den Einsatz von Charts gibt es ein externes Modul.

    Bei reaktiven Anwendungen sowohl Charts als auch OutsystemsCharts Module verwendet, weil keine Preparation möglich. Bei Pie Chart müssen die Daten beim Laden der Seite geladen werden -> Lösung mit Server Aktion, die ruft Server-Aktionen von Charts Modul auf

7. Benutzerdefinierten Code integrieren

    1. Integration Studio konfigurieren

        17.03.2022

        Benutzerdefinierter Code kann mit Integration Studio integriert werden. Eine Erweiterung kann nur in Form einer Serveraktion, Entität oder Struktur implementiert und veröffentlicht werden. Erweiterungen können sowohl in .NET als auch in J2EE entwickelt werden.
        -> Allerdings wird Java in Outsystems 11 nicht mehr unterstützt!
        Referenz: https://www.outsystems.com/forums/discussion/41629/java-with-integration-tool-11/ 
        Als IDE kann hier nur Microsoft Visual Studio verwendet werden.
        Serveraktionen können sowohl importiert als auch neu erstellt werden.
        Auch externe Datenbanken können als Erweiterung in der Anwendung zur Verfügung gestellt werden. Dazu muss zunächst eine Verbindung zur Datenbank im Service Center hergestellt werden.
        Mit Integration Studio ist es möglich, ausgewählte Tabellen aus der externen Datenbank auf eine interne Datenbank abzubilden und als Erweiterung zu veröffentlichen.

    2. Dashboard Werte über erste einfache Server Aktion über Integration Studio berechnen

        17.03.2022

        Serveraktionen können als klassische Funktionen mit Ein- und Ausgabeparametern entwickelt werden. (aber auch keine Funktion: mehrere Ausgänge) Bei der Definition von Datentypen gibt es nur eine begrenzte Anzahl von Datentypen. Beispielsweise können generische Klassen nicht als Datentypen definiert werden.
        In meinem Fall möchte ich eine Liste von Objekten als Eingabeparameter definieren, aber diese Art von Datentyp existiert nicht.
        -> Es besteht die Möglichkeit, als Datentyp eine Record List zu wählen. Allerdings muss man dann den Datensatz unter Entities neu definieren, was insgesamt nicht gut ist. Wenn diese Erweiterung in Service Studio integriert wird, wird eine neue Tabelle für den Datensatz erstellt -> nicht sinnvoll. Sinnvoller wäre ein Mapping auf die vorhandene Tabelle
        -> Neuer Versuch als Struktur, dann Struktur im Service Studio mit Daten füllen
            -> Struktur darf nicht verändert werden

        Ich habe mich dafür entschieden, eine passende Funktion zu definieren. In diesem Fall soll die Funktion einfache Quoten(Zugesagte-, Abgelehnt-, Offene Angebote) berechnen.

        Problem:
        Expression kann nicht über direkten Weg mit Hilfe einer Server Aktion berechnet werden

        Lösung
        https://www.outsystems.com/forums/discussion/65986/server-action-not-avaliable-in-expressions/

5. Sonstiges

    * Wenn man im Tab Interface auf den obersten Eintrag klickt kann man in den Properties die Informatioenn zum Modul sehen. 
