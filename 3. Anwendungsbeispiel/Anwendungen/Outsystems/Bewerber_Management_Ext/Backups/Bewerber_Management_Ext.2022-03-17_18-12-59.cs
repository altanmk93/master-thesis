using System;
using System.Collections;
using System.Data;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.RuntimePublic.Db;

namespace OutSystems.NssBewerber_Management_Ext {

	public class CssBewerber_Management_Ext: IssBewerber_Management_Ext {

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sszusage_quote"></param>
		/// <param name="ssbewerber"></param>
		/// <param name="ssabsage_quote"></param>
		/// <param name="sszusagen"></param>
		/// <param name="ssoffen_quote"></param>
		/// <param name="ssoffen"></param>
		public void MssBewerberung_quote(out decimal sszusage_quote, int ssbewerber, out decimal ssabsage_quote, int sszusagen, out decimal ssoffen_quote, int ssoffen, int ssabsagen) {
			if (sszusagen != 0)
            {
				sszusage_quote = (decimal) sszusagen / ssbewerber;

			} else
            {
				sszusage_quote = 0;

			}

			if (ssabsagen != 0)
			{
				ssabsage_quote = (decimal) ssabsagen / ssbewerber;

			}
			else
			{
				ssabsage_quote = 0;

			}

			if (ssoffen != 0)
			{
				ssoffen_quote = (decimal) ssoffen / ssbewerber;

			}
			else
			{
				ssoffen_quote = 0;

			}


			// TODO: Write implementation for action
		} // MssBewerberung_quote


	} // CssBewerber_Management_Ext

} // OutSystems.NssBewerber_Management_Ext

