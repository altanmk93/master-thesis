using System;
using System.Collections;
using System.Data;
using OutSystems.HubEdition.RuntimePlatform;

namespace OutSystems.NssBewerber_Management_Ext {

	public interface IssBewerber_Management_Ext {

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sszusage_quote"></param>
		/// <param name="ssbewerber"></param>
		/// <param name="ssabsage_quote"></param>
		/// <param name="sszusagen"></param>
		/// <param name="ssoffen_quote"></param>
		/// <param name="ssoffen"></param>
		/// <param name="ssabsagen"></param>
		void MssBewerberung_quote(out int sszusage_quote, int ssbewerber, out int ssabsage_quote, int sszusagen, out int ssoffen_quote, int ssoffen, int ssabsagen);

	} // IssBewerber_Management_Ext

} // OutSystems.NssBewerber_Management_Ext
