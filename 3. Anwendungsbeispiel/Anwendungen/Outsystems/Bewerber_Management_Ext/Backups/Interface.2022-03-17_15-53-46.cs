using System;
using System.Collections;
using System.Data;
using OutSystems.HubEdition.RuntimePlatform;

namespace OutSystems.NssBewerber_Management_Ext {

	public interface IssBewerber_Management_Ext {

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ssAmount"></param>
		/// <param name="ssBewerber"></param>
		void MssBewerber_Amount(out int ssAmount, RLBewerberRecordList ssBewerber);

	} // IssBewerber_Management_Ext

} // OutSystems.NssBewerber_Management_Ext
