using System;
using System.Collections;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;

namespace OutSystems.NssBewerber_Management_Ext {

	/// <summary>
	/// Structure <code>RCBewerberRecord</code>
	/// </summary>
	[Serializable()]
	public partial struct RCBewerberRecord: ISerializable, ITypedRecord<RCBewerberRecord> {
		internal static readonly GlobalObjectKey IdBewerber = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*13Ts1wjlCboffXeJ4_m_gA");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Bewerber")]
		public ENBewerberEntityRecord ssENBewerber;


		public static implicit operator ENBewerberEntityRecord(RCBewerberRecord r) {
			return r.ssENBewerber;
		}

		public static implicit operator RCBewerberRecord(ENBewerberEntityRecord r) {
			RCBewerberRecord res = new RCBewerberRecord(null);
			res.ssENBewerber = r;
			return res;
		}

		public BitArray ChangedAttributes {
			set {
				ssENBewerber.ChangedAttributes = value;
			}
			get {
				return ssENBewerber.ChangedAttributes;
			}
		}
		public BitArray OptimizedAttributes;

		public RCBewerberRecord(params string[] dummy) {
			OptimizedAttributes = null;
			ssENBewerber = new ENBewerberEntityRecord(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = new BitArray(5, false);
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
					ssENBewerber.OptimizedAttributes = GetDefaultOptimizedValues()[0];
				} else {
					ssENBewerber.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = ssENBewerber.OptimizedAttributes;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssENBewerber.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCBewerberRecord r) {
			this = r;
		}


		public static bool operator == (RCBewerberRecord a, RCBewerberRecord b) {
			if (a.ssENBewerber != b.ssENBewerber) return false;
			return true;
		}

		public static bool operator != (RCBewerberRecord a, RCBewerberRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCBewerberRecord)) return false;
			return (this == (RCBewerberRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssENBewerber.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCBewerberRecord(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssENBewerber = new ENBewerberEntityRecord(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssENBewerber", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssENBewerber' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssENBewerber = (ENBewerberEntityRecord) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssENBewerber.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssENBewerber.InternalRecursiveSave();
		}


		public RCBewerberRecord Duplicate() {
			RCBewerberRecord t;
			t.ssENBewerber = (ENBewerberEntityRecord) this.ssENBewerber.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssENBewerber.ToXml(this, recordElem, "Bewerber", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "bewerber") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Bewerber")) variable.Value = ssENBewerber; else variable.Optimized = true;
				variable.SetFieldName("bewerber");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			return ssENBewerber.ChangedAttributeGet(key);
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			return ssENBewerber.OptimizedAttributeGet(key);
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdBewerber) {
				return ssENBewerber;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssENBewerber.FillFromOther((IRecord) other.AttributeGet(IdBewerber));
		}
		public bool IsDefault() {
			RCBewerberRecord defaultStruct = new RCBewerberRecord(null);
			if (this.ssENBewerber != defaultStruct.ssENBewerber) return false;
			return true;
		}
	} // RCBewerberRecord
}
