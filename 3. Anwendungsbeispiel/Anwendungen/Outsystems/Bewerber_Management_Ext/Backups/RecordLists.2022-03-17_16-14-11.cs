using System;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;
using OutSystems.HubEdition.RuntimePlatform.NewRuntime;

namespace OutSystems.NssBewerber_Management_Ext {

	/// <summary>
	/// RecordList type <code>RLBewerberRecordList</code> that represents a record list of
	///  <code>Bewerber</code>
	/// </summary>
	[Serializable()]
	public partial class RLBewerberRecordList: GenericRecordList<RCBewerberRecord>, IEnumerable, IEnumerator, ISerializable {
		public static void EnsureInitialized() {}

		protected override RCBewerberRecord GetElementDefaultValue() {
			return new RCBewerberRecord("");
		}

		public T[] ToArray<T>(Func<RCBewerberRecord, T> converter) {
			return ToArray(this, converter);
		}

		public static T[] ToArray<T>(RLBewerberRecordList recordlist, Func<RCBewerberRecord, T> converter) {
			return InnerToArray(recordlist, converter);
		}
		public static implicit operator RLBewerberRecordList(RCBewerberRecord[] array) {
			RLBewerberRecordList result = new RLBewerberRecordList();
			result.InnerFromArray(array);
			return result;
		}

		public static RLBewerberRecordList ToList<T>(T[] array, Func <T, RCBewerberRecord> converter) {
			RLBewerberRecordList result = new RLBewerberRecordList();
			result.InnerFromArray(array, converter);
			return result;
		}

		public static RLBewerberRecordList FromRestList<T>(RestList<T> restList, Func <T, RCBewerberRecord> converter) {
			RLBewerberRecordList result = new RLBewerberRecordList();
			result.InnerFromRestList(restList, converter);
			return result;
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RLBewerberRecordList(): base() {
		}

		/// <summary>
		/// Constructor with transaction parameter
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLBewerberRecordList(IDbTransaction trans): base(trans) {
		}

		/// <summary>
		/// Constructor with transaction parameter and alternate read method
		/// </summary>
		/// <param name="trans"> IDbTransaction Parameter</param>
		/// <param name="alternateReadDBMethod"> Alternate Read Method</param>
		[Obsolete("Use the Default Constructor and set the Transaction afterwards.")]
		public RLBewerberRecordList(IDbTransaction trans, ReadDBMethodDelegate alternateReadDBMethod): this(trans) {
			this.alternateReadDBMethod = alternateReadDBMethod;
		}

		/// <summary>
		/// Constructor declaration for serialization
		/// </summary>
		/// <param name="info"> SerializationInfo</param>
		/// <param name="context"> StreamingContext</param>
		public RLBewerberRecordList(SerializationInfo info, StreamingContext context): base(info, context) {
		}

		public override BitArray[] GetDefaultOptimizedValues() {
			BitArray[] def = new BitArray[1];
			def[0] = null;
			return def;
		}
		/// <summary>
		/// Create as new list
		/// </summary>
		/// <returns>The new record list</returns>
		protected override OSList<RCBewerberRecord> NewList() {
			return new RLBewerberRecordList();
		}


	} // RLBewerberRecordList
}
