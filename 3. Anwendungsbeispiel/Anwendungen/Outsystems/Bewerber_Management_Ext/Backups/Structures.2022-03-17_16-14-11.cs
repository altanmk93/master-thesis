using System;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Runtime.Serialization;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;

namespace OutSystems.NssBewerber_Management_Ext {

	/// <summary>
	/// Structure <code>STBewerberStructure</code> that represents the Service Studio structure
	///  <code>Bewerber</code> <p> Description: </p>
	/// </summary>
	[Serializable()]
	public partial struct STBewerberStructure: ISerializable, ITypedRecord<STBewerberStructure>, ISimpleRecord {
		internal static readonly GlobalObjectKey IdVorname = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*XUtwyK+HO0WbO4ZVmjAlSQ");
		internal static readonly GlobalObjectKey IdName = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*zlDOl9Hr3E+zRoI_DRk8OQ");
		internal static readonly GlobalObjectKey IdGeburtsdatum = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*_2CkjLPCaEqkS3M_mkM+6Q");
		internal static readonly GlobalObjectKey IdEmail = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*WALvwH3Vd0ybkDzsBMuEhg");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Vorname")]
		public string ssVorname;

		[System.Xml.Serialization.XmlElement("Name")]
		public string ssName;

		[System.Xml.Serialization.XmlElement("Geburtsdatum")]
		public DateTime ssGeburtsdatum;

		[System.Xml.Serialization.XmlElement("Email")]
		public string ssEmail;


		public BitArray OptimizedAttributes;

		public STBewerberStructure(params string[] dummy) {
			OptimizedAttributes = null;
			ssVorname = "";
			ssName = "";
			ssGeburtsdatum = new DateTime(1900, 1, 1, 0, 0, 0);
			ssEmail = "";
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssVorname = r.ReadText(index++, "Bewerber.Vorname", "");
			ssName = r.ReadText(index++, "Bewerber.Name", "");
			ssGeburtsdatum = r.ReadDate(index++, "Bewerber.Geburtsdatum", new DateTime(1900, 1, 1, 0, 0, 0));
			ssEmail = r.ReadText(index++, "Bewerber.Email", "");
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(STBewerberStructure r) {
			this = r;
		}


		public static bool operator == (STBewerberStructure a, STBewerberStructure b) {
			if (a.ssVorname != b.ssVorname) return false;
			if (a.ssName != b.ssName) return false;
			if (a.ssGeburtsdatum != b.ssGeburtsdatum) return false;
			if (a.ssEmail != b.ssEmail) return false;
			return true;
		}

		public static bool operator != (STBewerberStructure a, STBewerberStructure b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(STBewerberStructure)) return false;
			return (this == (STBewerberStructure) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssVorname.GetHashCode()
				^ ssName.GetHashCode()
				^ ssGeburtsdatum.GetHashCode()
				^ ssEmail.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public STBewerberStructure(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssVorname = "";
			ssName = "";
			ssGeburtsdatum = new DateTime(1900, 1, 1, 0, 0, 0);
			ssEmail = "";
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssVorname", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssVorname' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssVorname = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("ssName", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssName' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssName = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("ssGeburtsdatum", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssGeburtsdatum' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssGeburtsdatum = (DateTime) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("ssEmail", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssEmail' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssEmail = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public STBewerberStructure Duplicate() {
			STBewerberStructure t;
			t.ssVorname = this.ssVorname;
			t.ssName = this.ssName;
			t.ssGeburtsdatum = this.ssGeburtsdatum;
			t.ssEmail = this.ssEmail;
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Structure");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Vorname")) VarValue.AppendAttribute(recordElem, "Vorname", ssVorname, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Vorname");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Name")) VarValue.AppendAttribute(recordElem, "Name", ssName, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Name");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Geburtsdatum")) VarValue.AppendAttribute(recordElem, "Geburtsdatum", ssGeburtsdatum, detailLevel, TypeKind.Date); else VarValue.AppendOptimizedAttribute(recordElem, "Geburtsdatum");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Email")) VarValue.AppendAttribute(recordElem, "Email", ssEmail, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Email");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "vorname") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Vorname")) variable.Value = ssVorname; else variable.Optimized = true;
			} else if (head == "name") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Name")) variable.Value = ssName; else variable.Optimized = true;
			} else if (head == "geburtsdatum") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Geburtsdatum")) variable.Value = ssGeburtsdatum; else variable.Optimized = true;
			} else if (head == "email") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Email")) variable.Value = ssEmail; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdVorname) {
				return ssVorname;
			} else if (key == IdName) {
				return ssName;
			} else if (key == IdGeburtsdatum) {
				return ssGeburtsdatum;
			} else if (key == IdEmail) {
				return ssEmail;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssVorname = (string) other.AttributeGet(IdVorname);
			ssName = (string) other.AttributeGet(IdName);
			ssGeburtsdatum = (DateTime) other.AttributeGet(IdGeburtsdatum);
			ssEmail = (string) other.AttributeGet(IdEmail);
		}
		public bool IsDefault() {
			STBewerberStructure defaultStruct = new STBewerberStructure(null);
			if (this.ssVorname != defaultStruct.ssVorname) return false;
			if (this.ssName != defaultStruct.ssName) return false;
			if (this.ssGeburtsdatum != defaultStruct.ssGeburtsdatum) return false;
			if (this.ssEmail != defaultStruct.ssEmail) return false;
			return true;
		}
	} // STBewerberStructure

} // OutSystems.NssBewerber_Management_Ext
