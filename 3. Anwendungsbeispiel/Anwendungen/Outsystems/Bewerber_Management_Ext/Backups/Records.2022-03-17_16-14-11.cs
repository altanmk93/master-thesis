using System;
using System.Collections;
using System.Data;
using System.Runtime.Serialization;
using System.Reflection;
using System.Xml;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;

namespace OutSystems.NssBewerber_Management_Ext {

	/// <summary>
	/// Structure <code>RCBewerberRecord</code>
	/// </summary>
	[Serializable()]
	public partial struct RCBewerberRecord: ISerializable, ITypedRecord<RCBewerberRecord> {
		internal static readonly GlobalObjectKey IdBewerber = GlobalObjectKey.Parse("2UmDmepsh0WSfJ_D1JexCA*1HlxXkPu6fLFRZRyU4+XDA");

		public static void EnsureInitialized() {}
		[System.Xml.Serialization.XmlElement("Bewerber")]
		public STBewerberStructure ssSTBewerber;


		public static implicit operator STBewerberStructure(RCBewerberRecord r) {
			return r.ssSTBewerber;
		}

		public static implicit operator RCBewerberRecord(STBewerberStructure r) {
			RCBewerberRecord res = new RCBewerberRecord(null);
			res.ssSTBewerber = r;
			return res;
		}

		public BitArray OptimizedAttributes;

		public RCBewerberRecord(params string[] dummy) {
			OptimizedAttributes = null;
			ssSTBewerber = new STBewerberStructure(null);
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[1];
			all[0] = null;
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
					ssSTBewerber.OptimizedAttributes = value[0];
				}
			}
			get {
				BitArray[] all = new BitArray[1];
				all[0] = null;
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssSTBewerber.Read(r, ref index);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(RCBewerberRecord r) {
			this = r;
		}


		public static bool operator == (RCBewerberRecord a, RCBewerberRecord b) {
			if (a.ssSTBewerber != b.ssSTBewerber) return false;
			return true;
		}

		public static bool operator != (RCBewerberRecord a, RCBewerberRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(RCBewerberRecord)) return false;
			return (this == (RCBewerberRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssSTBewerber.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public RCBewerberRecord(SerializationInfo info, StreamingContext context) {
			OptimizedAttributes = null;
			ssSTBewerber = new STBewerberStructure(null);
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("ssSTBewerber", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named 'ssSTBewerber' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				ssSTBewerber = (STBewerberStructure) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
			ssSTBewerber.RecursiveReset();
		}

		public void InternalRecursiveSave() {
			ssSTBewerber.InternalRecursiveSave();
		}


		public RCBewerberRecord Duplicate() {
			RCBewerberRecord t;
			t.ssSTBewerber = (STBewerberStructure) this.ssSTBewerber.Duplicate();
			t.OptimizedAttributes = null;
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Record");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
			}
			if (detailLevel > 0) {
				ssSTBewerber.ToXml(this, recordElem, "Bewerber", detailLevel - 1);
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "bewerber") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Bewerber")) variable.Value = ssSTBewerber; else variable.Optimized = true;
				variable.SetFieldName("bewerber");
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			throw new Exception("Method not Supported");
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == IdBewerber) {
				return ssSTBewerber;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			if (other == null) return;
			ssSTBewerber.FillFromOther((IRecord) other.AttributeGet(IdBewerber));
		}
		public bool IsDefault() {
			RCBewerberRecord defaultStruct = new RCBewerberRecord(null);
			if (this.ssSTBewerber != defaultStruct.ssSTBewerber) return false;
			return true;
		}
	} // RCBewerberRecord
}
