using System;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Runtime.Serialization;
using OutSystems.ObjectKeys;
using OutSystems.RuntimeCommon;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.HubEdition.RuntimePlatform.Db;
using OutSystems.Internal.Db;

namespace OutSystems.NssBewerber_Management_Ext {

	public class ENBewerberEntityConfiguration {
		private static object config;
		private static string PhysicalTableName {
			get {
				try {
					Type EntityConfiguration = Type.GetType("OutSystems.HubEdition.RuntimePlatform.Db.EntityConfiguration,OutSystems.HubEdition.RuntimePlatform");
					if (EntityConfiguration != null) {
						if (config == null) {
							config = EntityConfiguration.GetMethod("GetEntityConfiguration", BindingFlags.Public | BindingFlags.Static).Invoke(null, new object[] { "4c4d24bc-ad46-4973-9cc3-777f61e398dc", "263cc402-1093-4f2d-b4b3-6e636816fdbe", "Bewerber", "Bewerber"}); 
						}
						return EntityConfiguration.GetProperty("PhysicalTableName").GetValue(config).ToString();
					} else {
						return "Bewerber"; 
					}
				} catch {
					return "Bewerber"; 
				}
			}
		}
		public static string GetPhysicalTableName() {
			return PhysicalTableName; 
		}
	}

	public sealed partial class ENBewerberEntity {
		private static readonly System.Collections.Generic.Dictionary<string, string> entityAttributes = new System.Collections.Generic.Dictionary<string, string>() {
			{ "id", "id"
			}
			, { "vorname", "Vorname"
			}
			, { "name", "Name"
			}
			, { "geburtsdatum", "Geburtsdatum"
			}
			, { "email", "Email"
			}
		};
		public static System.Collections.Generic.Dictionary<string, string> AttributesToDatabaseNamesMap() {
			return entityAttributes;
		}
		public static string AttributeDatabaseName(string attributeName) {
			string databaseName;
			entityAttributes.TryGetValue(attributeName, out databaseName);
			return databaseName;
		}
		public static string LocalViewName(int? tenant, string locale) {
			return ViewName(null, locale);
		}
		public static string ViewName(int? tenant, string locale) {
			return ENBewerberEntityConfiguration.GetPhysicalTableName();
		}
	} // ENBewerberEntity

	/// <summary>
	/// Entity <code>ENBewerberEntityRecord</code> that represents the Service Studio entity
	///  <code>Bewerber</code> <p> Description: </p>
	/// </summary>
	[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityRecordDetails("Bewerber", "AsQ8JpMQLU+0s25jaBb9vg", "vCRNTEatc0mcw3d_YeOY3A", 0, "Bewerber", null)]
	[Serializable()]
	public partial struct ENBewerberEntityRecord: ISerializable, ITypedRecord<ENBewerberEntityRecord>, ISimpleRecord {
		internal static readonly GlobalObjectKey Idid = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*dyRDLCQTmka+QsurfFz9IQ");
		internal static readonly GlobalObjectKey IdVorname = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*ywP5Ymy03k2ETz8CK4d8pg");
		internal static readonly GlobalObjectKey IdName = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*w7_PTYhKqEW53OduqgNLnw");
		internal static readonly GlobalObjectKey IdGeburtsdatum = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*CIgE9vj8U0+MfQyCJ0AmUA");
		internal static readonly GlobalObjectKey IdEmail = GlobalObjectKey.Parse("vCRNTEatc0mcw3d_YeOY3A*NeGjmAH5kEONkPAL5BLcLA");

		public static void EnsureInitialized() {}
		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("id", 0, false, false, true, true)]
		[System.Xml.Serialization.XmlElement("id")]
		private long _ssid;
		public long ssid {
			get {
				return _ssid;
			}
			set {
				if ((_ssid!=value) || OptimizedAttributes[0]) {
					ChangedAttributes[0] = true;
					_ssid = value;
				}
			}
		}

		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("Vorname", 50, false, false, false, true)]
		[System.Xml.Serialization.XmlElement("Vorname")]
		private string _ssVorname;
		public string ssVorname {
			get {
				return _ssVorname;
			}
			set {
				if ((_ssVorname!=value) || OptimizedAttributes[1]) {
					ChangedAttributes[1] = true;
					_ssVorname = value;
				}
			}
		}

		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("Name", 50, false, false, false, true)]
		[System.Xml.Serialization.XmlElement("Name")]
		private string _ssName;
		public string ssName {
			get {
				return _ssName;
			}
			set {
				if ((_ssName!=value) || OptimizedAttributes[2]) {
					ChangedAttributes[2] = true;
					_ssName = value;
				}
			}
		}

		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("Geburtsdatum", 0, false, false, false, true)]
		[System.Xml.Serialization.XmlElement("Geburtsdatum")]
		private DateTime _ssGeburtsdatum;
		public DateTime ssGeburtsdatum {
			get {
				return _ssGeburtsdatum;
			}
			set {
				if ((_ssGeburtsdatum!=value) || OptimizedAttributes[3]) {
					ChangedAttributes[3] = true;
					_ssGeburtsdatum = value;
				}
			}
		}

		[OutSystems.HubEdition.RuntimePlatform.MetaInformation.EntityAttributeDetails("Email", 50, false, false, false, true)]
		[System.Xml.Serialization.XmlElement("Email")]
		private string _ssEmail;
		public string ssEmail {
			get {
				return _ssEmail;
			}
			set {
				if ((_ssEmail!=value) || OptimizedAttributes[4]) {
					ChangedAttributes[4] = true;
					_ssEmail = value;
				}
			}
		}


		public BitArray ChangedAttributes;

		public BitArray OptimizedAttributes;

		public ENBewerberEntityRecord(params string[] dummy) {
			ChangedAttributes = new BitArray(5, true);
			OptimizedAttributes = new BitArray(5, false);
			_ssid = 0L;
			_ssVorname = "";
			_ssName = "";
			_ssGeburtsdatum = new DateTime(1900, 1, 1, 0, 0, 0);
			_ssEmail = "";
		}

		public BitArray[] GetDefaultOptimizedValues() {
			BitArray[] all = new BitArray[0];
			return all;
		}

		public BitArray[] AllOptimizedAttributes {
			set {
				if (value == null) {
				} else {
				}
			}
			get {
				BitArray[] all = new BitArray[0];
				return all;
			}
		}

		/// <summary>
		/// Read a record from database
		/// </summary>
		/// <param name="r"> Data base reader</param>
		/// <param name="index"> index</param>
		public void Read(IDataReader r, ref int index) {
			ssid = r.ReadEntityReferenceLongInteger(index++, "Bewerber.id", 0L);
			ssVorname = r.ReadText(index++, "Bewerber.Vorname", "");
			ssName = r.ReadText(index++, "Bewerber.Name", "");
			ssGeburtsdatum = r.ReadDate(index++, "Bewerber.Geburtsdatum", new DateTime(1900, 1, 1, 0, 0, 0));
			ssEmail = r.ReadText(index++, "Bewerber.Email", "");
			ChangedAttributes = new BitArray(5, false);
			OptimizedAttributes = new BitArray(5, false);
		}
		/// <summary>
		/// Read from database
		/// </summary>
		/// <param name="r"> Data reader</param>
		public void ReadDB(IDataReader r) {
			int index = 0;
			Read(r, ref index);
		}

		/// <summary>
		/// Read from record
		/// </summary>
		/// <param name="r"> Record</param>
		public void ReadIM(ENBewerberEntityRecord r) {
			this = r;
		}


		public static bool operator == (ENBewerberEntityRecord a, ENBewerberEntityRecord b) {
			if (a.ssid != b.ssid) return false;
			if (a.ssVorname != b.ssVorname) return false;
			if (a.ssName != b.ssName) return false;
			if (a.ssGeburtsdatum != b.ssGeburtsdatum) return false;
			if (a.ssEmail != b.ssEmail) return false;
			return true;
		}

		public static bool operator != (ENBewerberEntityRecord a, ENBewerberEntityRecord b) {
			return !(a==b);
		}

		public override bool Equals(object o) {
			if (o.GetType() != typeof(ENBewerberEntityRecord)) return false;
			return (this == (ENBewerberEntityRecord) o);
		}

		public override int GetHashCode() {
			try {
				return base.GetHashCode()
				^ ssid.GetHashCode()
				^ ssVorname.GetHashCode()
				^ ssName.GetHashCode()
				^ ssGeburtsdatum.GetHashCode()
				^ ssEmail.GetHashCode()
				;
			} catch {
				return base.GetHashCode();
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context) {
			Type objInfo = this.GetType();
			FieldInfo[] fields;
			fields = objInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			for (int i = 0; i < fields.Length; i++)
			if (fields[i] .FieldType.IsSerializable)
			info.AddValue(fields[i] .Name, fields[i] .GetValue(this));
		}

		public ENBewerberEntityRecord(SerializationInfo info, StreamingContext context) {
			ChangedAttributes = new BitArray(5, true);
			OptimizedAttributes = new BitArray(5, false);
			_ssid = 0L;
			_ssVorname = "";
			_ssName = "";
			_ssGeburtsdatum = new DateTime(1900, 1, 1, 0, 0, 0);
			_ssEmail = "";
			Type objInfo = this.GetType();
			FieldInfo fieldInfo = null;
			fieldInfo = objInfo.GetField("_ssid", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssid' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssid = (long) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("_ssVorname", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssVorname' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssVorname = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("_ssName", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssName' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssName = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("_ssGeburtsdatum", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssGeburtsdatum' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssGeburtsdatum = (DateTime) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
			fieldInfo = objInfo.GetField("_ssEmail", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
			if (fieldInfo == null) {
				throw new Exception("The field named '_ssEmail' was not found.");
			}
			if (fieldInfo.FieldType.IsSerializable) {
				_ssEmail = (string) info.GetValue(fieldInfo.Name, fieldInfo.FieldType);
			}
		}

		public void RecursiveReset() {
		}

		public void InternalRecursiveSave() {
		}


		public ENBewerberEntityRecord Duplicate() {
			ENBewerberEntityRecord t;
			t._ssid = this._ssid;
			t._ssVorname = this._ssVorname;
			t._ssName = this._ssName;
			t._ssGeburtsdatum = this._ssGeburtsdatum;
			t._ssEmail = this._ssEmail;
			t.ChangedAttributes = new BitArray(5);
			t.OptimizedAttributes = new BitArray(5);
			for (int i = 0; i < 5; i++) {
				t.ChangedAttributes[i] = ChangedAttributes[i];
				t.OptimizedAttributes[i] = OptimizedAttributes[i];
			}
			return t;
		}

		IRecord IRecord.Duplicate() {
			return Duplicate();
		}

		public void ToXml(Object parent, System.Xml.XmlElement baseElem, String fieldName, int detailLevel) {
			System.Xml.XmlElement recordElem = VarValue.AppendChild(baseElem, "Entity");
			if (fieldName != null) {
				VarValue.AppendAttribute(recordElem, "debug.field", fieldName);
				fieldName = fieldName.ToLowerInvariant();
			}
			if (detailLevel > 0) {
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".id")) VarValue.AppendAttribute(recordElem, "id", ssid, detailLevel, TypeKind.EntityReferenceLongInteger); else VarValue.AppendOptimizedAttribute(recordElem, "id");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Vorname")) VarValue.AppendAttribute(recordElem, "Vorname", ssVorname, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Vorname");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Name")) VarValue.AppendAttribute(recordElem, "Name", ssName, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Name");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Geburtsdatum")) VarValue.AppendAttribute(recordElem, "Geburtsdatum", ssGeburtsdatum, detailLevel, TypeKind.Date); else VarValue.AppendOptimizedAttribute(recordElem, "Geburtsdatum");
				if (!VarValue.FieldIsOptimized(parent, fieldName + ".Email")) VarValue.AppendAttribute(recordElem, "Email", ssEmail, detailLevel, TypeKind.Text); else VarValue.AppendOptimizedAttribute(recordElem, "Email");
			} else {
				VarValue.AppendDeferredEvaluationElement(recordElem);
			}
		}

		public void EvaluateFields(VarValue variable, Object parent, String baseName, String fields) {
			String head = VarValue.GetHead(fields);
			String tail = VarValue.GetTail(fields);
			variable.Found = false;
			if (head == "id") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".id")) variable.Value = ssid; else variable.Optimized = true;
			} else if (head == "vorname") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Vorname")) variable.Value = ssVorname; else variable.Optimized = true;
			} else if (head == "name") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Name")) variable.Value = ssName; else variable.Optimized = true;
			} else if (head == "geburtsdatum") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Geburtsdatum")) variable.Value = ssGeburtsdatum; else variable.Optimized = true;
			} else if (head == "email") {
				if (!VarValue.FieldIsOptimized(parent, baseName + ".Email")) variable.Value = ssEmail; else variable.Optimized = true;
			}
			if (variable.Found && tail != null) variable.EvaluateFields(this, head, tail);
		}

		public bool ChangedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(Idid)) {
				return ChangedAttributes[0];
			} else if (key.Equals(IdVorname)) {
				return ChangedAttributes[1];
			} else if (key.Equals(IdName)) {
				return ChangedAttributes[2];
			} else if (key.Equals(IdGeburtsdatum)) {
				return ChangedAttributes[3];
			} else if (key.Equals(IdEmail)) {
				return ChangedAttributes[4];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public bool OptimizedAttributeGet(GlobalObjectKey key) {
			if (key.Equals(Idid)) {
				return OptimizedAttributes[0];
			} else if (key.Equals(IdVorname)) {
				return OptimizedAttributes[1];
			} else if (key.Equals(IdName)) {
				return OptimizedAttributes[2];
			} else if (key.Equals(IdGeburtsdatum)) {
				return OptimizedAttributes[3];
			} else if (key.Equals(IdEmail)) {
				return OptimizedAttributes[4];
			} else {
				throw new Exception("Invalid key");
			}
		}

		public object AttributeGet(GlobalObjectKey key) {
			if (key == Idid) {
				return ssid;
			} else if (key == IdVorname) {
				return ssVorname;
			} else if (key == IdName) {
				return ssName;
			} else if (key == IdGeburtsdatum) {
				return ssGeburtsdatum;
			} else if (key == IdEmail) {
				return ssEmail;
			} else {
				throw new Exception("Invalid key");
			}
		}
		public void FillFromOther(IRecord other) {
			ChangedAttributes = new BitArray(5);
			OptimizedAttributes = new BitArray(5);
			if (other == null) return;
			ssid = (long) other.AttributeGet(Idid);
			ChangedAttributes[0] = other.ChangedAttributeGet(Idid);
			OptimizedAttributes[0] = other.OptimizedAttributeGet(Idid);
			ssVorname = (string) other.AttributeGet(IdVorname);
			ChangedAttributes[1] = other.ChangedAttributeGet(IdVorname);
			OptimizedAttributes[1] = other.OptimizedAttributeGet(IdVorname);
			ssName = (string) other.AttributeGet(IdName);
			ChangedAttributes[2] = other.ChangedAttributeGet(IdName);
			OptimizedAttributes[2] = other.OptimizedAttributeGet(IdName);
			ssGeburtsdatum = (DateTime) other.AttributeGet(IdGeburtsdatum);
			ChangedAttributes[3] = other.ChangedAttributeGet(IdGeburtsdatum);
			OptimizedAttributes[3] = other.OptimizedAttributeGet(IdGeburtsdatum);
			ssEmail = (string) other.AttributeGet(IdEmail);
			ChangedAttributes[4] = other.ChangedAttributeGet(IdEmail);
			OptimizedAttributes[4] = other.OptimizedAttributeGet(IdEmail);
		}
		public bool IsDefault() {
			ENBewerberEntityRecord defaultStruct = new ENBewerberEntityRecord(null);
			if (this.ssid != defaultStruct.ssid) return false;
			if (this.ssVorname != defaultStruct.ssVorname) return false;
			if (this.ssName != defaultStruct.ssName) return false;
			if (this.ssGeburtsdatum != defaultStruct.ssGeburtsdatum) return false;
			if (this.ssEmail != defaultStruct.ssEmail) return false;
			return true;
		}
	} // ENBewerberEntityRecord

} // OutSystems.NssBewerber_Management_Ext
