using System;
using System.Collections;
using System.Data;
using OutSystems.HubEdition.RuntimePlatform;
using OutSystems.RuntimePublic.Db;

namespace OutSystems.NssBewerber_Management_Ext {

	public class CssBewerber_Management_Ext: IssBewerber_Management_Ext {

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sszusage_quote"></param>
		/// <param name="ssbewerber"></param>
		/// <param name="ssabsage_quote"></param>
		/// <param name="sszusagen"></param>
		/// <param name="ssoffen_quote"></param>
		/// <param name="ssoffen"></param>
		public void MssBewerberung_quote(out int sszusage_quote, int ssbewerber, out int ssabsage_quote, int sszusagen, out int ssoffen_quote, int ssoffen, int ssabsagen) {
			if (sszusagen != 0)
            {
				sszusage_quote = ssbewerber / sszusagen;

			} else
            {
				sszusage_quote = 0;

			}

			if (ssabsagen != 0)
			{
				ssabsage_quote = ssbewerber / ssabsagen;

			}
			else
			{
				ssabsage_quote = 0;

			}

			if (ssoffen != 0)
			{
				ssoffen_quote = ssbewerber / ssoffen;

			}
			else
			{
				ssoffen_quote = 0;

			}


			// TODO: Write implementation for action
		} // MssBewerberung_quote


	} // CssBewerber_Management_Ext

} // OutSystems.NssBewerber_Management_Ext

