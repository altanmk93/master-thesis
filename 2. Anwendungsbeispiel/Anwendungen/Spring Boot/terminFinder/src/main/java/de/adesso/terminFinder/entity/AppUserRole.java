package de.adesso.terminFinder.entity;

public enum AppUserRole {

    USER,
    ADMIN
}
