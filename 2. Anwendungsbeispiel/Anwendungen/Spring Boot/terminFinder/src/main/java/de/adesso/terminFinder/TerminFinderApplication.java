package de.adesso.terminFinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TerminFinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(TerminFinderApplication.class, args);
	}

}
