1. Recherche Authentifizierung in PowerApps

    24.12.2021T15:05-16:30

    Es gibt die Möglichkeit, in einer Canvas-App eine benutzerdefinierte Login-Seite mit einer einfachen SharePoint-Liste bestehend aus beliebigen Benutzern und entsprechenden Passwörtern zu erstellen, jedoch soll in diesem Anwendungsbeispiel die Integration unterschiedlicher Identity Provider untersucht werden. Daher ist diese Möglichkeit ausgeschlossen.

    Beispiele für Login und Signup

    Login: 
    * https://www.spguides.com/powerapps-create-login-screen/
    * https://www.youtube.com/watch?v=Y6wdEsnrGQg

    signup:
    * https://collab365.com/how-to-create-a-user-registration-form-using-powerapps-flow-sharepoint-lists/


    Um einen Identity Provider einzubinden, ist es notwendig, eine Portal-App zu erstellen.

    Referenz:

    * https://docs.microsoft.com/de-de/powerapps/maker/portals/configure/use-simplified-authentication-configuration

2. Portal-App erstellen

    24.12.2021T16:35-17:00

    Übersicht: https://docs.microsoft.com/de-de/powerapps/maker/portals/overview

    Das Erstellen einer Portal-App nimmt in der Regel einige Minuten in Anspruch. Portal-Apps sind extern sichtbare Websites, die es Benutzern außerhalb der Organisation im Gegensatz zu Canvas-Apps ermöglichen, sich mit unterschiedlichen Identitäten anzumelden.
    Eine Portal-App bietet standardmäßig eine Registrierungs- und Anmeldeseite.
    Auch externe Identitätsanbieter können eingerichtet werden. Azure AD ist standardmäßig eingerichtet.
    Auf der Startseite kann man die Sicherheit der App-Portale über die Anwendung „Portal Management“ steuern.

    Problem: Vorschauansicht kann im Privatmodus des Webbrowser nicht geladen werden

    Lösung: kein Privatmodus, neuer Webbrowser
    Referenz: https://powerusers.microsoft.com/t5/Power-Apps-Portals/Unable-to-load-preview-power-apps-portal-sync-configuration-page/td-p/928269


3. Google SSO aktivieren

    30.12.2021T13:35-13:50

    Um Google als Identitätsanbieter zu aktivieren, werden die Client-ID und das Client-Secret einer Google-App benötigt. Ich habe die Anleitung unten 1:1 befolgt. Insgesamt war es schnell einzurichten.
    Voraussetzung: Google Cloud Account

    Anleitung: 
    https://vblogs.in/sign-in-with-google-account-in-powerapps-portal/


4. Terminanfrage integrieren und sichtbar für Nutzer machen

    09.01.2022T  (Dauer:2h)
    Problem: Formular kann nicht auf der Seite angezeigt werden und er erscheint folgende Fehlermeldung:
    "You don't have the appropriate permissions."
    -> Berechtigungen für Tabelle hinzufügen!
    Lösung: Berechtigung kann direkt über den Punkt Tabellenberechtigungen in der Seitenleiste verwaltet werden (Bearbeiten Modus)

5. Beziehung zwischen Termin Entität und Portal-Nutzern erstellen

    20.01.2022

    Portal-App-Nutzer kann man unter Contacts finden. Jedoch befinden die registrierten in der User Tabelle.
    Problem: Wie kann man eine Datenbankquery bauen um alle User zu filtern, die sich bei dieser Applikation angemeldet haben?
    Beziehung zu Systemtabellen kann erstellt werden! -> lieber: benutzer in separate tabelle speichern

6.  Untersuchung der Testmöglichkeiten

    12.01.2022T (Dauer 2h)

    Nach längerer Recherche existiert ein von Microsoft entwickeltes Test Framework mit dem Namen Test Studio für End-To-End Tests für die Benutzeroberfläche der Canvas Apps.
    Da es aktuell keine Möglichkeit gibt Portal Applikationen zu testen, habe ich mich dafür entschieden einen Test für das erste Anewendungsbeispiel zu entwickeln und alle Erkenntisse zu dokumentieren.

3. Sonstiges

    * App Versionen können unter den Details eingesehen werden
    * Power Apps Portale nach außen gerichtete Websites, so dass Benutzer außerhalb des Unternehmens sich zu anzumelden
    * Landing Page Canvas App festlegen: Der Erste Screen in der linken Navigationsleiste
    ( Referenz: https://powerusers.microsoft.com/t5/Building-Power-Apps/Landing-Screen-of-the-App/td-p/36689 )
    * Nutzer die sich über eine Portal App registrieren findet man unter Security Contacts All Contacts