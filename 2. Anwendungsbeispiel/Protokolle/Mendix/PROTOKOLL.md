1. Anmeldeseite erstellen

    22.12.2021T10:00-11:30

    Mendix bietet standardmäßig eine Anmeldeseite(Single Sign On), um sich mit seinem Mendix-Konto anzumelden, vorausgesetzt, die App wird in der Mendix Cloud deployed. Ich habe mich dazu beschlossen, eine eigene Login-Seite zu erstellen, damit sich auch keine Mendix-Benutzer einloggen können.
    (Die Aktivierung von Mendix Single Sign On wird hier beschrieben:
    https://docs.mendix.com/developerportal/deploy/mendix-sso)

    Standardmäßig gibt es Widgets für die Authentifizierung. Das Widget „Login id text box“ stellt das Eingabefeld für den Benutzernamen und „Password text box“ das Eingabefeld für das Passwort dar. Es gibt auch eine Anmeldeschaltfläche. An dieser Stelle können auch Validierungsnachrichten eingefügt werden.

    Es ist wichtig, dass Sicherheitsanpassungen vorgenommen werden müssen. In den Sicherheitseinstellungen der Applikation muss zunächst die Sicherheitsstufe auf Prototyp / Demo oder Produktion eingestellt werden, damit grundsätzlich ein Login möglich ist.
    Damit die Anwendung für jeden Benutzer sichtbar ist, gibt es eine Benutzerrolle namens "Anonymous" (Anonym), damit die Anwendung ohne Anmeldung aufgerufen werden kann. Es ist daher notwendig, in den Sicherheitseinstellungen den Zugriff für anonyme Benutzer sicherzustellen. Meldet man sich dann mit seinem Konto an, wechselt man die Benutzerrolle von "Anonymous" zu "User"

    Eine weitere Einstellung muss in der Navigation vorgenommen werden. Im Punkt Hauptseite muss man zunächst die Homepage angeben, und die rollenbasierte Homepage festlegen. An dieser Stelle ist es notwendig, die Rolle "Anonym" zuzuweisen. 
    Außerdem gibt es noch den Punkt Authentifizierung, wo man dann die Anmeldeseite festlegen kann.

    In der Navigation muss eine andere Einstellung vorgenommen werden. In der Einstellung "Home pages" muss man zunächst die Startseite eingeben und die rollenbasierte Startseite definieren. An dieser Stelle ist es notwendig, die Rolle "Anonymous" zuzuweisen.
    Es gibt auch eine weitere Einstellung für der Authentifizierung, wo man dann die Anmeldeseite festlegen kann.

    Referenz: 
    * https://docs.mendix.com/refguide/anonymous-users 
    * https://www.youtube.com/watch?v=ePuS-grmcXw 



2. User Management/ Benutzerrollen Konzepte verstehen

    22.12.2021T12:30-15:15

    Standardmäßig existiert nur der Administrator Benutzer. Seinen Benutzernamen und das zugehörige Passwort man unter den Sicherheitseinstellungen unter Administrator einsehen. 

    Um Authentifizierung von Nutzern und Autorisierung eines Zugriffs zu vestehen, habe ich mir Domain Model unter System in der linken Sidebar angeschaut. Es existiert die Entität "User" welche die Benutzer und die Entität "UserRole" welche die Benutzerrollen darstellen.

    Unter den Marketplace Modulen existiert ein hilfreiches Modul "Administration", welches den gesamten Ablauf der Nutzerverwaltung erleichtert. Von diesem Modul sollte man Gebrauch machen.
    Auch hier habe ich in das Domainen Modell reingeschaut, welches überschaubar aufgebaut ist. Es existiert eine Entität Account, welches eine Generalisierung vom System.User ist. Es gibt eine nicht persistente Entität "AccountPasswordData" zum zurücksetzen von Passwörtern.

    Standardmäßig existiert nur der Administratorbenutzer. Seinen Benutzernamen und das zugehörige Passwort man unter den Sicherheitseinstellungen unter Administrator einsehen. 

    Um die Authentifizierung von Benutzern und die Autorisierung des Zugriffs auf Ressourcen zu verstehen, habe ich mir das Domänenmodell unter System in der linken Seitenleiste angesehen. Es gibt die Entität "User", die die Benutzer repräsentiert, und die Entität "UserRole", die die Benutzerrollen repräsentiert.

    Zu den Marktplatz-Modulen gehört ein hilfreiches Modul „Administration“, das den gesamten Prozess der Benutzerverwaltung vereinfacht. Von diesem Modul sollte man Gebrauch machen.
    Auch hier habe ich mir das Domänenmodell angeschaut, das klar strukturiert ist. Es gibt ein Entität "Account", das eine Generalisierung von System.User ist. Es gibt eine nicht persistente Entität "AccountPasswordData" zum Zurücksetzen von Passwörtern.

2. Registrierungsseite + Microflow erstellen

    22.12.2021T15:30-18:30

    Ich habe ein Formular für die Registrierungsseite erstellt und als Datenquelle die Entität "AnonymousUser" zugewiesen. Alle notwendigen Felder werden automatisch generiert.
    Die Registrierungsfunktion kann man eigenständig entwickeln. An dieser Stelle wird ein Microflow verwendet.
    In den folgenden Forenbeiträgen habe ich viele hilfreiche Tipps mitgenommen, um den Microflow zu entwickeln.

    * https://forum.mendix.com/link/questions/93869
    * https://forum.mendix.com/link/questions/16052
    * https://forum.mendix.com/link/questions/94441 
    * https://forum.mendix.com/link/questions/96936


    Wichtig ist, die Werte der Eingabefelder aus dem Registrierungsformular den Attributen der Account-Entität zuzuordnen und über eine Datenbankabfrage die Benutzerrolle eines Accounts zu ermitteln. Letzteres kann über die Objektaktivität "Retrieve" realisiert werden. Ein Account-Objekt und das referenzierte AccountPasswordData-Objekt müssen erstellt und anschließend persistiert werden. Zu beachten ist, dass das persistente Account-Objekt dem referenzierten AccountPasswordData-Objekt zugewiesen werden muss.

3. Sichtbarkeit für verschiedene Rollen einstellen

    22.12.2021T18:30-18:45

    Die Sichtbarkeit der Seiten für die verschiedenen Benutzerrollen kann in der Cloud-basierten Lösung nicht eingestellt werden, sondern nur in Mendix Studio Pro.
    In der App-spezifischen Sicherheitseinstellung kann man die Sichtbarkeit für die Benutzerrollen einstellen. Es ist zu beachten, dass die Anmeldeseite für alle Benutzer mit der Rolle „Anonymous“ sichtbar sein muss.

4. Integration von Google als Identitäsanbieter

    05.01.2022(2h)

    OIDC Module downloaden und importieren

    Referenz: 
    https://medium.com/mendix/add-google-sso-to-your-mendix-app-using-the-oidc-module-a76784b2690f 

    OIDC letzte Version enthält viele Errors!
    -> Fehler wurden selbsständig mit Hilfe von Vorschlägen behoben (Dauer: 10 Minuten)

    Google App erstellen (Google Cloud Platform)

    Vorsicht vor Modulen aus dem Marketplace

    Nach mehreren Versuchen mit anderen Modulen ist es mir nicht gelungen Google als Identitätsanbieter zu integrieren

    Es gibt eine weitere Möglichkeit über das Control Center von Mendix versucht
    -> nicht gegeben mit Testversion
        -> Anfrage wurde gestellt, um Zugang zum Kontrollzentrum zu kriegen -> wurde zur Verfügung gestellt
        -> hierbei handelt es sich erneut um das OICD Modul, welches in der Mendix Cloud integriert ist. Hiermit wird gewährleistet, eine Anmeldung bei der Mendix cloud mit einem anderen Identitätsanbieter durchzuführen

    05.01.2022 (3h)
    letzer Versuch erneut mit OIDC Modul, weil es von vielen Mendix Entwicklern empfohlen wurde.
    -> Google SSO wurde erfoglreich integriert
    -> Das Modul enthält eine Dokumentation, die hilfreich war aber nicht selbsterklärend
    -> Google redirect URI funktioniert nicht für lokale Entwicklung!

5. Termin Anfrage

    22.01.2022T (3-4h)

    Bei der List View kann die Datenquelle aus verschieden Möglichkeiten extrahiert werden(Datenbak, XPATh, Microflow, Nanoflow, Association)

    Checkbox set selector versucht -> nicht funktioniert

    dann erneut versucht -> hat funktioniert

    Problem: Referenziertes Attribut wird in data view nicht angezeigt
    Lösung: https://docs.mendix.com/refguide/input-reference-set-selector 
    Bootstrap Multi Select(Jedoch Beziehung 1:*)


6. Sonstiges

* Customizing von Modul(Atlas_Core) Elementen (z.B. Sidebar) möglich unter Marketplace
* Rückgabewert von Microflow kann im Endpunkt festgelegt werden(roter Kreis)
* Erkennen und Auflösen von Performance Problemen(Laufzeitanalysen): https://docs.mendix.com/howto/monitoring-troubleshooting/detect-and-resolve-performance-issues 
