1. Registrierungsseite erstellen

    28.12.2021T12:30-13:05

    Outsystems hat standardmäßig eine Anmeldeseite integriert. Unter dem Reiter "Interface" findet man Anmeldeseite. Dort habe ich einen Button hinzugefügt, welcher auf die Registrierungsseite weiterleitet, wenn man ihn drückt. Es ist jedoch notwendig selbständig eine Registrierungsseite zu erstellen. Unter dem Reiter "Data" kann man die Datenbank der "Systems" schauen. Dort gibt es eine Entität "User", welche die Benutzer darstellt. Zunächst habe ich eine blanke Webseite erstellt, ein Formular als Widget hinzugefügt und per Drag & Drop die "User" Enität in das Formular hineingeschoben. Die notwendigen Eingabefelder haben sich automatisch generiert. Jedoch muss man für das Passwort selbständig ein Eingabefeld erstellen.  

    Outsystems hat standardmäßig eine Anmeldeseite integriert. Die Anmeldeseite findet man unter dem Reiter "Interface". Dort habe ich eine Schaltfläche hinzugefügt, die beim Drücken auf die Registrierungsseite umleitet. Es ist jedoch notwendig, selbst eine Registrierungsseite zu erstellen. Unter dem Reiter "Data" kann die Datenbank "System" eingesehen werden. Es gibt eine Entität namens "User", die die Benutzer repräsentiert. Zuerst habe ich eine leere Seite erstellt, ein Formular als Widget hinzugefügt und die Entität "User" per Drag & Drop in das Formular gezogen. Die notwendigen Eingabefelder wurden automatisch generiert. Man muss jedoch selbst ein Eingabefeld für das Passwort erstellen.

2. Screen Action für Registrierungslogik erstellen

    28.12.2021T13:05-13:30

    Die Logik für die Registrierung wurde mit einer Screen Action implementiert. In dieser Screen Action muss man zunächst das übergebene Passwort verschlüsseln. Dazu benötigt man eine Dependency. Unter dem Menüpunkt „Manage Dependencies“ kann man nach "usersLibrary" suchen und die Serveraktion „EncryptPassword“ installieren. Erst dann kann diese Serveraktion im Flow verwendet werden. Anschließend muss das verschlüsselte Passwort dem entsprechenden Attribut des Objekts zugeordnet und anschließend das Objekt gespeichert werden.
    
    Referenz:
    * https://www.youtube.com/watch?v=z_0A-fdBGe8
    * https://success.outsystems.com/Documentation/11/Developing_an_Application/Secure_the_Application/End_User_Management/End_Users_Authentication/Single_Sign-On 


3. Google als Identitäsanbieter integrieren

    06.01.2022T11:25-14:35

    Es existieren viele Module aus dem Marketplace zum Installieren jedoch besitzen sie meistens keine Dokumentation

    1. Google SignIn Reactive

        Dieses Modul ist nicht sehr geeignet gewesen, weil sie nur eine Schaltfläche mit clientseitiger Google-Anmeldung anbietet. Mit der Anmeldung wird kein Outsystems Benutzer angelegt. Es bietet auch keine Möglichkeit, sich abzumelden.

    2. IdP  

        Lässt sich nicht installieren, da es Fehler enthält. 
    
    3. Google Services OAuth 2.0

        Einige UI Flows lassen sich nicht installieren.

    4. Es existiert eine offizielle Übung von Outsystems zum Integrieren von Google OAuth
        
        07.01.2022T10:50-14:05  

        https://www.outsystems.com/training/lesson/1839/integrating-with-oauth-exercise/

        Hier ist das Modul Google Services OAuth2 bereits vollständig integriert und funktionstüchtig.

        Bevor ich mit der Entwicklung starten konnte, ist die Erstellung einer Google App vorausgesetzt. Aus der erstellten Google App werden die Client ID und Client Secret benötigt. Die Übung wurde vollständig umgesetzt.

        Referenz: https://www.youtube.com/watch?v=NvjoXtT7w-g

        
    5. Nächster Schritt: Teile der Übung in die TerminFinder Applikation integrieren

        10.01.2022T11:30-13:45

        Teile der Dependencies aus der Übung lassen sich nicht installieren weil sie nur für tradionelle Webapplikationen geeignet sind. Ich habe jedoch eine reaktive Webapplikation erstellt!
        Lösung: Tradionelle Web Applikation erstellen. Jedoch muss ich nun die gesamte Registrierungslogik erneut umsetzen.

    6. Authentifizierung und Autorisierung umsetzen

        11.01.2022T10:15-11:20

        Als ersten Schritt war es notwendig den Callback selbständig umzusetzen, weil die Dependency es nicht mitliefert. Der Callback ist an dieser Stelle für die Weiterleitung auf den Homescreen nach Authentifizierung notwendig.

        11.01.2022T12:00-15:05

        Während der Entwicklung ist mir aufgefallen, dass folgendes in der URL stand."(Not.Licensed.For.Production)="
        Die Lösung für dieses Problem war, dass diese Seite für nicht autorisierte Benutzer nicht sichtbar war, sodass ich den Zugriff für Seite der Rolle Anoynm erlauben müsste.

        Registrierungsformular:
        Beim Drag & drop einer lokalen Variable die eine Entität darstellt in das Formular wird ein Formular zum Editieren eines Datensatzes erstellt. Dies ist unterschiedlich zu reaktiven Applikation, die an dieser Stelle ein Formular zum Anlegen eines Datensatzes erstellen.

        Die Lösung für dieses Problem ist ein eigenes Formular erstellen. Die Felder des Formular werden den Attributen der lokalen User Objekts zugewiesen und zum Speichern eines Benutzers wurde eine Screen Action erstellt.

        Es gibt wesentliche Unterschiede zwischen tradionellen und reaktiven Webanwendungen.

4. Termin Anfrage 

    1. TerminFinder-Benutzer erstellen

        13.01.2022T15:00-17:03

        Mehrere Benutzer sollen zu einem Termin eingeladen werden können. Eine Beziehung zu Systemtabellen (Tabelle System.User) kann nicht hergestellt werden. Eine Lösung bestand darin, eine neue Tabelle zu erstellen, in der alle Benutzer gespeichert sind, die sich über die TerminFinder Applikation registrieren.

        14.01.2022T10:05-10:50

        Beim Erstellen eines TerminFinder-Benutzers gab es das Problem, dass doppelte Einträge in der Tabelle TerminFinderUser erstellt wurden. Es gibt keine Einstellungsoption, um Felder als eindeutig festzulegen. Eine Lösung bestand darin, ein Aggregat zu schreiben, das zuerst prüft, ob der Benutzer bereits existiert.

    2. Einladung mehrerer TerminFinder-Benutzer ermöglichen 

        14.01.2022T11:30-15:00

        Ich habe nach einer Anzeigeoption gesucht, um mehrere Benutzer eine Einladung senden zu können. Ich habe einige Module ausprobiert, die die Auswahl mehrerer Elemente aus einem Dropdown-Menü implementieren sollten. Diese Module waren jedoch entweder nicht mit traditionellen Webanwendungen kompatibel oder enthielten Fehler. Daher habe ich mich entschieden, alle TerminFinder-Benutzer (bis auf den eingeloggten Benutzer) aufzulisten und die einzuladenden Benutzer mit einer Checkbox auszuwählen. Für diese Implementierung war es notwendig, eine neue Struktur anzulegen und diese mit der TerminFinderUser-Tabelle zu verknüpfen. Die Erweiterung enthält den Status, ob der Benutzer eingeladen werden soll.

        In traditionellen Webanwendungen können sogenannte Vorbereitungsaktionen (Preparation Action) ausgeführt werden, wenn Logik ausgeführt werden muss, bevor der Bildschirm geladen wird. Dies war an dieser Stelle notwendig, um die Liste aller TerminFinderUser zu generieren.

        Referenz:
        * https://www.youtube.com/watch?v=uP9vJVH8PsY 


    3. Terminanfrage Server Aktion finalisieren

        16.01.2022T13:00-16:50

        Es gibt viele Systemfunktionen wie einen ListFilter oder ListAppends etc. Die Schwierigkeit bei der Implementierung des Flows für die Terminanfrage war die Abbildung der Variablen auf die richtigen Datenstrukturen. Es sollte immer darauf geachtet werden, welchen Datentyp der Rückgabewert ein Aggregat hat, um eine entsprechende Zuordnung vornehmen zu können.       


4. BDD Test Framework zum Testen der Authentifizierung

    12.01.2022T10:45-13:30

    * Als erstes BDD Framework installieren
    * Neue BDD Applikation erstellen
    * TerminFinder Server Aktionen öffentlich stellen, damit sie als Applikation installiert werden können 
    * Server Aktionen als Dependency in der der BDD Applikation installieren

    13.01.2022T10:15-14:30

    Problem: Google OAuth2 benötigt eine Weiterleitungs-URI, damit sich der Benutzer auf der Google-Anmeldeseite anmelkan kann und dann zur eigentlichen Anwendung weitergeleitet wird. Nach der Weiterleitung wird ein Code generiert, der zur Generierung der googleapi-Tokens notwendig ist.
    Diese Art der Anfrage ist jedoch ohne Weiterleitung nicht realisierbar.
    Der Code wird erst nach erfolgreicher Authentifizierung auf der Google-Anmeldeseite generiert.

    Ich habe den Workaround versucht, den Code über den klassischen Weg zu genieren, anschließend habe ich den Code aus der URL kopiert und eine Anfrage mit den anderen Parametern gesendet, jedoch wurde diese Anfrage von der Google Api abgelehnt.

    19.01.2022T13:45-15:10

    Die Authentifizierungslogik über Google kann daher nicht im Unit-Test abgebildet werden. Jedoch gibt es einen weiteren Flow, der nach erfolgreicher Authentifizierung die persönlichen Daten mit Hilfe von Token-Informationen abfragt und sich dann in der Anwendung anmeldet. Es ist möglich, diesen Flow in Unit-Tests einzubeziehen. Die Token-Informationen können als Mock-Objekt gespeichert werden, indem die Token-Informationen vorab über den klassischen Authentifizierungsprozess generiert werden können.

    Ein BDD-Test besteht aus einem beschriebenen Szenario und den durchgeführten Schritten "Given", "When" und "Then". Das Framework bietet spezielle Tools (BDDScenario, BDDStep) um einen Test zu implementieren. Wichtig ist, dass jeder Schritt zunächst verbal beschrieben und mit einer Bildschirmaktion umgesetzt wird.

    Wichtig:
    Für Unit-Tests mit dem BDD-Framework können nur Serveraktionen integriert werden.
    
7. Sonstiges

    * Test Typen:
        * Functional UI Tests: Selenium
        * Akzeptanztests Akzeptanz Test Driven Development: Robot Framework
        * Integrationstests
        * Unit Tests / Komponenententest: Test Automator, Test Framework, BDD Framework, Unit Testing Framework
    * Aggregate müssen bei tradionellen Webapplikation in einer Server Aktion aufgerufen werden
    * Screen Aktionen können nicht öffentlich sein
    * Vor der Installation eines externen Dependency prüft Outsystems, ob Komplikationen in der Applikation entstehen können
    * Laufzeitanalysen nur mit Architektur Dashboard möglich: 
    Architektur Dashboard ist nicht enthalten in Testversion. Die IDE von Outsystems gibt jedoch Hinweise, wenn es in einem zu Performanceeinbüßen kommen kann (z.B. Einsatz von mehreren Server Aktionen in einem Logic Flow)
    mehr dazu hier:  https://www.outsystems.com/evaluation-guide/out-of-the-box-performance-optimization/

    https://www.outsystems.com/de-de/evaluation-guide/performance-dashboards-and-apis/
    
    https://success.outsystems.com/Documentation/11/Managing_the_Applications_Lifecycle/Manage_technical_debt/How_does_Architecture_Dashboard_work 

    https://success.outsystems.com/Documentation/11/Managing_the_Applications_Lifecycle/Manage_technical_debt/Code_Patterns
